//
//  AppDelegate.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/10/15.
//  Copyright (c) 2015 Agilabs. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Fabric
import Crashlytics
import ECSlidingViewController
import Parse
import RNActivityView

let googleAPIKey = "AIzaSyCTBxW6oxZBOeOknwxioW-BtNTVAaHCWG8"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var slidingViewController: ECSlidingViewController!
    var deviceToken = ""
    
    class func sharedAppDelegate() -> AppDelegate! {
        return UIApplication.sharedApplication().delegate as? AppDelegate
    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent;
        
        // Google configs
        GMSServices.provideAPIKey(googleAPIKey)
        
        Fabric.with([Crashlytics.self])
        
        Parse.setApplicationId("h2cCFuME5WqjCTKpwJaXcCWxlr69Pwa5RdJPGrQj",
            clientKey: "yQfhL0yE62CJuIJvpeJQeQi0D6EMKOUOR0s5H1Gx")
        
        DataManager.sharedInstance.getLocalCategories()
        
        let userDefault = NSUserDefaults.standardUserDefaults()
        let userId = userDefault.integerForKey(FoodTrackerUserIdKey)
        let userToken = userDefault.valueForKey(FoodTrackerUserTokenKey)
        let deviceToken = userDefault.stringForKey(FoodTrackerDeviceTokenKey) ?? ""
        if deviceToken.characters.count > 0 {
            self.deviceToken = deviceToken
        }
        
        if (userToken != nil) {
            let user = User()
            user.userId = userId
            user.token = userToken as? String ?? ""
            
            DataManager.sharedInstance.loggedUser = user
            HTTPClient.sharedInstance.setHeaderToken(user.token)
            
            DataManager.sharedInstance.getUserInfoById(user.userId, token: user.token, completionHandler: { (result, error) -> Void in
                // Do nothing
            })
            
            setHome()
        }
        else {
            setWelCome()
        }
        
        // Register for Push Notitications
        if application.applicationState != UIApplicationState.Background {
            // Track an app open here if we launch with a push, unless
            // "content_available" was used to trigger a background push (introduced in iOS 7).
            // In that case, we skip tracking here to avoid double counting the app-open.
            
            let preBackgroundPush = !application.respondsToSelector("backgroundRefreshStatus")
            let oldPushHandlerOnly = !self.respondsToSelector("application:didReceiveRemoteNotification:fetchCompletionHandler:")
            var pushPayload = false
            if let options = launchOptions {
                pushPayload = options[UIApplicationLaunchOptionsRemoteNotificationKey] != nil
            }
            if (preBackgroundPush || oldPushHandlerOnly || pushPayload) {
                PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
            }
        }
        if application.respondsToSelector("registerUserNotificationSettings:") {
            let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        FBSDKAppEvents.activateApp()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(application: UIApplication,
        openURL url: NSURL,
        sourceApplication: String?,
        annotation: AnyObject) -> Bool {
            let user = DataManager.sharedInstance.loggedUser;
            
            // Only open truck detail if user already logged in
            if url.scheme == "co.tastytrackr" && !user.token.isEmpty {
                let keyValues = url.query?.componentsSeparatedByString("=")
                if let keyValues = keyValues {
                    if let truckId: Int = Int(keyValues[1]) {
                        pushTruckDetailWithId(truckId)
                    }
                }
                
                return true
            }
            
            return FBSDKApplicationDelegate.sharedInstance().application(
                application,
                openURL: url,
                sourceApplication: sourceApplication,
                annotation: annotation)
    }
    
    // MARK: - Helper methods
    func setHome() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let homeNC = storyboard.instantiateViewControllerWithIdentifier("HomeNC")
        let menuVC = storyboard.instantiateViewControllerWithIdentifier("MenuViewController")
        slidingViewController = ECSlidingViewController(topViewController: homeNC)
        slidingViewController.underLeftViewController = menuVC
        self.slidingViewController.anchorLeftRevealAmount = UIScreen.mainScreen().bounds.width-50;
        self.slidingViewController.anchorRightPeekAmount = 50
        homeNC.view.addGestureRecognizer(self.slidingViewController.panGesture)
        self.window?.rootViewController = self.slidingViewController
    }
    
    func setWelCome() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let welcomeNC = storyboard.instantiateViewControllerWithIdentifier("WelcomeNCID")
        self.window?.rootViewController = welcomeNC
    }
    
    func pushTruckDetailWithId(truckId: Int) {
        window?.showActivityView()
        DataManager.sharedInstance.getTruckerDetailById(truckId,
            completionHandler: { (result, error) -> Void in
                self.window?.hideActivityView()
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let detailVC = storyboard.instantiateViewControllerWithIdentifier("TruckerDetailVC") as? TruckerDetailViewController
                if let detailVC = detailVC {
                    detailVC.trucker = result as? Trucker
                    let homeNC = self.slidingViewController.topViewController as? NormalNavigationController
                    homeNC?.pushViewController(detailVC, animated: false)
                }
        })
    }
    
    // MARK: - Private methods
    private func initializeNotificationServices() -> Void {
        let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }

    private func convertDeviceTokenToString(deviceToken:NSData) -> String {
        //  Convert binary Device Token to a String (and remove the <,> and white space charaters).
        var deviceTokenStr = deviceToken.description.stringByReplacingOccurrencesOfString(">", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        deviceTokenStr = deviceTokenStr.stringByReplacingOccurrencesOfString("<", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
        deviceTokenStr = deviceTokenStr.stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)

        return deviceTokenStr
    }
    
    // MARK: - Notification methods
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        // ...register device token with our Time Entry API server via REST
        let installation = PFInstallation.currentInstallation()
        installation.setDeviceTokenFromData(deviceToken)
        installation.saveInBackground()
        
        self.deviceToken = convertDeviceTokenToString(deviceToken)
        print(self.deviceToken)
        let userDefault = NSUserDefaults.standardUserDefaults()
        userDefault.setValue(self.deviceToken, forKey: FoodTrackerDeviceTokenKey)
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        if error.code == 3010 {
            print("Push notifications are not supported in the iOS Simulator.")
        }
        else {
            print("application:didFailToRegisterForRemoteNotificationsWithError: %@", error)
        }
    }
    
    // foreground (or if the app was in the background and the user clicks on the notification).
    /*
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
        fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // display the userInfo
        if let notification = userInfo["aps"] as? NSDictionary,
            let alert = notification["alert"] as? String {
                print(alert)
                /*
                var alertCtrl = UIAlertController(title: "Time Entry", message: alert as String, preferredStyle: UIAlertControllerStyle.Alert)
                alertCtrl.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                // Find the presented VC...
                var presentedVC = self.window?.rootViewController
                while (presentedVC!.presentedViewController != nil)  {
                    presentedVC = presentedVC!.presentedViewController
                }
                presentedVC!.presentViewController(alertCtrl, animated: true, completion: nil)
                
                // call the completion handler
                // -- pass in NoData, since no new data was fetched from the server.
                completionHandler(UIBackgroundFetchResult.NoData)
                */
        }
    }
    */
    func application(application: UIApplication,
        didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        print(userInfo)
        PFPush.handlePush(userInfo)
        if application.applicationState == UIApplicationState.Inactive {
            PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(userInfo)
            
            if let truckId: Int = userInfo["truck_id"] as? Int {
                pushTruckDetailWithId(truckId)
            }
        }
    }
}

