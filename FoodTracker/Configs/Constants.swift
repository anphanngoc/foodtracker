//
//  Constants.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/26/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

struct ScreenSize{
    static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
    static let IS_IPHONE_4_OR_LESS  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6          = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPAD              = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

// MARK: - API
let BASE_ENDPOINT     = "https://app.tastytrackr.co/api/v1/"
let ERROR_TITLE       = "We're sorry"
let FOOD_TRUCKER      = "TastyTrackr"

let CAMERA_TITLE                = "Camera"
let CAMERA_DESCRIPTION          = "Choose an option!"
let CAMERA_TAKE_A_PHOTO         = "Take a Photo"
let CAMERA_CAMERA_ROLL          = "Choose from Camera Roll"

//MARK: - ALERT
let ALERT_OK     = "OK"
let ALERT_CANCEL = "Cancel"

let FoodTrackerUserIdKey = "FoodTrackerUserIdKey";
let FoodTrackerUserTokenKey = "FoodTrackerUserTokenKey";
let FoodTrackerDeviceTokenKey = "FoodTrackerDeviceTokenKey";
let FoodTrackerFBAuthenKey = "FoodTrackerFBAuthenKey";

// Notifications
let FoodTrackerUpdateUserNotification = "FoodTrackerUpdateUserNotification"
let FoodTrackerUpdateTruckerNotification = "FoodTrackerUpdateTruckerNotification"

// Keys
let FB_PLACEMENT_ID = "430281907176022_430285873842292"