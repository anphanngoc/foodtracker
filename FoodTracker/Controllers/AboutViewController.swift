//
//  AboutViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/14/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import ECSlidingViewController

class AboutViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let menuTitles = ["About", "FAQ", "Terms & Conditions", "Privacy Policy", "Share App"];
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    @IBAction func menuButtonDidTouched(sender: AnyObject) {
        if self.slidingViewController().currentTopViewPosition == ECSlidingViewControllerTopViewPosition.AnchoredRight {
            self.slidingViewController().resetTopViewAnimated(true)
        }
        else {
            self.slidingViewController().anchorTopViewToRightAnimated(true);
        }
    }
    
    @IBAction func unwindToAboutVC(segue: UIStoryboardSegue) {
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PushWebVC" {
            if let webVC = segue.destinationViewController as? WebViewController {
                webVC.detailUrlString = (sender as? String)!
            }
        }
    }
}

extension AboutViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuTitles.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Configure the cell...
        var cell = tableView.dequeueReusableCellWithIdentifier("aboutCell")
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "aboutCell")
            cell?.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        
        cell!.textLabel?.text = menuTitles[indexPath.row]
        
        return cell!
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.textLabel?.font = UIFont.openSansRegular(size: 14)
        cell.textLabel?.textColor = UIColor.rgbColor(red: 48.0, green: 48.0, blue: 48.0)
    }
}

extension AboutViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView .deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.row == 0 {
            performSegueWithIdentifier("PushWebVC", sender: "about.html")
        }
        else if indexPath.row == 1 {
            performSegueWithIdentifier("PushWebVC", sender: "faq.html")
        }
        else if indexPath.row == 2 {
            performSegueWithIdentifier("PushWebVC", sender: "tnc.html")
        }
        else if indexPath.row == 3 {
            performSegueWithIdentifier("PushWebVC", sender: "privacy.html")
        }
        else if (indexPath.row == 4) {
            performSegueWithIdentifier("PushSharingVC", sender: nil)
        }
    }
}
