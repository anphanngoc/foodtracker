//
//  ChangePasswordViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/24/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var changePwdButton: UIButton!
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    
    var oldPwd = ""
    var pwdNew = ""
    var pwdConfirmation = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        avatarImageView.layer.cornerRadius = 45.0
        tableView.tableFooterView = UIView()
        changePwdButton.titleLabel?.font = UIFont.openSansRegular(size: 14)
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            var tmpFrame = topView.frame
            tmpFrame.size.height = 150.0
            topView.frame = tmpFrame
        }
        tableView.tableHeaderView = topView
        
        let avatar = DataManager.sharedInstance.loggedUser.avatar.large
        avatarImageView.kf_setImageWithURL(NSURL(string: avatar)!,
            placeholderImage: UIImage(named: "avatarDefault"))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Remove border in navigation bar
        navigationController?.navigationBar.setBackgroundImage(UIImage(),
            forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Remove border in navigation bar
        navigationController?.navigationBar.setBackgroundImage(nil,
            forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.shadowImage = nil
    }
    
    // MARK: - IBActions
    @IBAction func saveButtonDidTouched(sender: AnyObject) {
        performChangePassword()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - Actions
    
    func textFieldDidChangedValue(textField: UITextField) {
        if textField.tag == 100 {
            oldPwd = textField.text ?? ""
        }
        else if textField.tag == 200 {
            pwdNew = textField.text ?? ""
        }
        else {
            pwdConfirmation = textField.text ?? ""
        }
    }
    
    func performChangePassword() {
        
        guard !pwdNew.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "New password is blank!")
            return
        }
        
        guard pwdNew.characters.count >= 6 else {
            showOkeyMessage(ERROR_TITLE, message: "New password less than 6 characters!")
            return
        }
        
        guard !pwdConfirmation.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "Password confirmation is blank!")
            return
        }
        
        guard pwdConfirmation.characters.count >= 6 else {
            showOkeyMessage(ERROR_TITLE, message: "Password confirmation less than 6 characters!")
            return
        }
        
        showLoading()
        DataManager.sharedInstance.changeUserPasswordWithOldPassword(oldPwd, pwdNew: pwdNew, pwdConfirmation: pwdConfirmation,
            completionHandler: { (result, error) -> Void in
                self.hideLoading()
                let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default,
                    handler: { (UIAlertAction) -> Void in
                    self.navigationController?.popViewControllerAnimated(true)
                })
                self.showAlertWithActions(FOOD_TRUCKER, message: Utilities.getErrorMessage(error), actions: [okAction])
        })
    }
}

extension ChangePasswordViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "InputCellID"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! InputTableViewCell
        
        cell.textField?.addTarget(self, action: "textFieldDidChangedValue:", forControlEvents: UIControlEvents.EditingChanged)
        if (indexPath.row == 0) {
            cell.titleLabel?.text = "Old Password"
            cell.textField?.placeholder = "Enter your name"
            cell.textField?.tag = 100
        }
        else if (indexPath.row == 1) {
            cell.titleLabel?.text = "New Password"
            cell.textField?.placeholder = "Enter your email"
            cell.textField?.tag = 200
        }
        else if (indexPath.row == 2) {
            cell.titleLabel?.text = "Confirm New Password"
            cell.textField?.placeholder = "Enter your phone"
            cell.textField?.delegate = self
            cell.textField?.returnKeyType = UIReturnKeyType.Send
        }
        
        return cell
    }
}

extension ChangePasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        performChangePassword()
        
        return true
    }
}

