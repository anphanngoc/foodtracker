//
//  CountryPickerViewController.swift
//  FoodTracker
//
//  Created by An Phan on 12/2/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import ObjectMapper

protocol CountryPickerViewControllerDelegate {
    func countryPickerDidSelectedCountry(country: CountryCode)
    func countryPickerDidCancelled()
}

class CountryPickerViewController: UIViewController {

    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var overlayView: UIView!
    
    var dialCode = ""
    var countries = [CountryCode]()
    var delegate: CountryPickerViewControllerDelegate?
    var selectedCountry: CountryCode?
    
    init() {
        let className = NSStringFromClass(self.dynamicType).componentsSeparatedByString(".").last
        super.init(nibName: className, bundle:nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        overlayView.backgroundColor = UIColor.rgbaColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        let contriesDict = NSArray.readArraybyFileJSON("countries")!
        self.countries = Mapper<CountryCode>().mapArray(contriesDict)!
        
        if dialCode.characters.count > 0 {
            let index = findingDialCodeIndex()
            selectedCountry = countries[index]
            pickerView.selectRow(index, inComponent: 0, animated: false)
        }
        else {
            let isoCode = getIsoCountryCode()
            for countryObject: CountryCode in countries {
                if isoCode.uppercaseString == countryObject.code {
                    dialCode = countryObject.dialCode
                    break
                }
            }
            
            let index = findingDialCodeIndex()
            selectedCountry = countries[index]
            pickerView.selectRow(index, inComponent: 0, animated: false)
        }
    }
    
    private func findingDialCodeIndex() -> Int {
        for (index, countryCode) in countries.enumerate() {
            if countryCode.dialCode == dialCode {
                return index
            }
        }
        
        return 0
    }
    
    // MARK: - IBActions
    @IBAction func doneButtonDidTouched(sender: AnyObject) {
        delegate?.countryPickerDidSelectedCountry(selectedCountry!)
        dismissViewControllerAnimated(false, completion: nil)
    }
    
    @IBAction func overlayViewDidTouched(sender: AnyObject) {
        delegate?.countryPickerDidCancelled()
        dismissViewControllerAnimated(false, completion: nil)
    }
}

extension CountryPickerViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let country = countries[row]
        let string = "(\(country.dialCode)) \(country.name)"
        return  string
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedCountry = countries[row]
        delegate?.countryPickerDidSelectedCountry(selectedCountry!)
    }
}
