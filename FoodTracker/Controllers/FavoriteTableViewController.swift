//
//  FavoriteTableViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/14/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import ECSlidingViewController

class FavoriteTableViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var adsView: UIView!
    
    var favoritedTruckers = NSMutableArray()
    var selectedTrucker: Trucker?
    
    // MARK: - Private vars
    private let maxLoading = 10
    private var currentPage: Int = 1
    private var isShowMore = false
    private var isLoadingMore = false
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: "refreshListOfFavoritedTruckers:", forControlEvents: UIControlEvents.ValueChanged)
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Table view
        tableView.tableFooterView = UIView(frame: CGRectMake(0.0, 0.0, CGRectGetWidth(view.frame), 50.0))
        tableView.registerNib(UINib(nibName: "FoodTruckerTableViewCell", bundle: nil),
            forCellReuseIdentifier: foodTruckCellID)
        tableView.registerClass(ShowMoreTableViewCell.self, forCellReuseIdentifier: ShowMoreCellID)
        tableView.addSubview(refreshControl)
        
        // Notification
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadTruckers",
            name: FoodTrackerUpdateTruckerNotification, object: nil)
        
        tableView.backgroundView = nil
        favoritedTruckers = DataManager.sharedInstance.favoritedTruckers
        if favoritedTruckers.count == 0 {
            showLoading()
        }
        DataManager.sharedInstance.getFavoritedTruckersWithPage(1, perPage: maxLoading) { (result, error) -> Void in
            if error != nil {
                self.hideLoading()
                self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
            }
            else {
                self.hideLoading()
                
                let currentTruckers = result as? [Trucker]
                self.isShowMore = currentTruckers?.count == self.maxLoading
                self.reloadTruckers()
            }
        }
        
        // Facebook Ads
        let adView = FBAdView(placementID: FB_PLACEMENT_ID, adSize: kFBAdSizeHeight50Banner, rootViewController: self)
        adView.delegate = self
        adView.loadAd()
        adsView.addSubview(adView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    @IBAction func unwindToFavoriteVC(segue: UIStoryboardSegue) {
    }

    @IBAction func menuButtonDidTouched(sender: AnyObject) {
        if self.slidingViewController().currentTopViewPosition == ECSlidingViewControllerTopViewPosition.AnchoredRight {
            self.slidingViewController().resetTopViewAnimated(true)
        }
        else {
            self.slidingViewController().anchorTopViewToRightAnimated(true);
        }
    }
    
    // MARK: - Helper methods
    
    func reloadTruckers() {
        favoritedTruckers = DataManager.sharedInstance.favoritedTruckers
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    func refreshListOfFavoritedTruckers(sender:AnyObject) {
        DataManager.sharedInstance.getFavoritedTruckersWithPage(1, perPage: maxLoading) { (result, error) -> Void in
            if error != nil {
                self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
            }
            else {
                let currentTruckers = result as? [Trucker]
                self.isShowMore = currentTruckers?.count == self.maxLoading
                
                self.reloadTruckers()
            }
        }
    }
    
    // MARK: - Private methods
    
    private func loadingMoreListOfTruckers() {
        currentPage += 1
        DataManager.sharedInstance.getFavoritedTruckersWithPage(currentPage, perPage: maxLoading) { (result, error) -> Void in
            if error != nil {
                self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
            }
            else {
                let currentTruckers = result as? [Trucker]
                self.isShowMore = currentTruckers?.count == self.maxLoading
                
                self.isLoadingMore = false
                self.reloadTruckers()
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PushTruckerDetailVC" {
            if let truckerDetailVC = segue.destinationViewController as? TruckerDetailViewController {
                truckerDetailVC.trucker = selectedTrucker
            }
        }
    }
}

extension FavoriteTableViewController: FBAdViewDelegate {
    
    func adViewDidLoad(adView: FBAdView) {
        print("adViewDidLoad")
        adsView.hidden = false
    }
    
    func adView(adView: FBAdView, didFailWithError error: NSError) {
        print("didFailWithError")
        adsView.hidden = true
    }
}

extension FavoriteTableViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoritedTruckers.count > 0 ? favoritedTruckers.count + 1 : 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == favoritedTruckers.count {
            // Initialize show more cell & display it
            var cell: ShowMoreTableViewCell? = tableView.dequeueReusableCellWithIdentifier(ShowMoreCellID)  as? ShowMoreTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: ShowMoreCellID) as? ShowMoreTableViewCell
            }
            
            cell!.separatorInset = UIEdgeInsetsMake(0, 160, 0, 160);
            if !(cell!.activityIndicator.isAnimating()) && isShowMore {
                cell!.activityIndicator.startAnimating()
            }
            
            return cell!
        }
        
        // Start load more neighbors
        if (indexPath.row == favoritedTruckers.count - 1 && isShowMore && !isLoadingMore) {
            isLoadingMore = true
            
            // Load more neighbor list data
            loadingMoreListOfTruckers()
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(foodTruckCellID, forIndexPath: indexPath) as! FoodTruckerTableViewCell
        
        let trucker = favoritedTruckers[indexPath.row] as! Trucker
        cell.nameLabel.text = trucker.truckername
        cell.addressLabel.text = trucker.truckerDecs
        cell.favoriteButton.selected = trucker.liked
        cell.rateButton.setTitle("\(trucker.averageRate)", forState: UIControlState.Normal)
        if trucker.truckerImages.count > 0 {
            let truckerImageUrl = trucker.truckerImages[0].image?.large
            cell.truckImageView.kf_setImageWithURL(NSURL(string: truckerImageUrl!)!)
        }
        else {
            cell.truckImageView.image = UIImage(named: "truckerPlaceholder")
        }
        
        return cell
    }
}

extension FavoriteTableViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        selectedTrucker = favoritedTruckers[indexPath.row] as? Trucker
        
        performSegueWithIdentifier("PushTruckerDetailVC", sender: nil)
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == favoritedTruckers.count {
            return isShowMore ? 44.0 : 0
        }
        
        return 75.0
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        tableView.backgroundView = nil
    }
}