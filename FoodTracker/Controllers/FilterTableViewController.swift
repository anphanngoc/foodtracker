//
//  FilterTableViewController.swift
//  FoodTracker
//
//  Created by An Phan on 11/24/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

protocol FilterTableViewControllerDelegate {
    func filterControllerDidSelectedCountry(country: Country)
    func filterControllerDidSelectedCity(city: City)
    func filterControllerDidSelectedCategory(category: Category)
    func filterControllerDidSelectedFoodType(foodType: FoodType)
}

class FilterTableViewController: UITableViewController {

    var countries = [Country]()
    var cities = [City]()
    var categories = NSArray()
    var foodTypeList = [FoodType]()
    
    var selectedCountry: Country?
    var selectedCity: City?
    var selectedCategory: Category?
    var selectedFoodType: FoodType?
    var isCountry = false
    var isCity = false
    var isTruckType = false
    var delegate: FilterTableViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /*
        if isCountry {
            selectedCountry = countries[0]
        }
        else {
            selectedCity = cities.count > 0 ? cities[0] : nil
        }
        */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isCountry {
            return countries.count
        }
        else if isCity {
            return cities.count
        }
        else if isTruckType {
            return categories.count
        }
        
        return foodTypeList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("categoryCellIdentifier", forIndexPath: indexPath)

        // Configure the cell...
        if isCountry {
            cell.textLabel?.text = countries[indexPath.row].countryName
        }
        else if isCity {
            cell.textLabel?.text = cities[indexPath.row].cityName
        }
        else if isTruckType {
            let category = categories[indexPath.row] as? Category
            cell.textLabel?.text = category?.categoryName
        }
        else {
            let foodType: FoodType = foodTypeList[indexPath.row]
            cell.textLabel?.text = foodType.foodTypeName
        }

        return cell
    }
    
    // MARK: - Table view delegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if isCountry {
            selectedCountry = countries[indexPath.row]
            delegate?.filterControllerDidSelectedCountry(self.selectedCountry!)
        }
        else if isCity {
            selectedCity = cities[indexPath.row]
            delegate?.filterControllerDidSelectedCity(selectedCity!)
        }
        else if isTruckType {
            selectedCategory = categories[indexPath.row] as? Category
            delegate?.filterControllerDidSelectedCategory(selectedCategory!)
        }
        else {
            selectedFoodType = foodTypeList[indexPath.row]
            delegate?.filterControllerDidSelectedFoodType(selectedFoodType!)
        }
    }
}
