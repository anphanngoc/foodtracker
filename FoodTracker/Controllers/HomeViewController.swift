//
//  HomeViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/11/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import ECSlidingViewController

class HomeViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var viewModeButton: UIBarButtonItem!
    @IBOutlet weak var searchButton: UIBarButtonItem!
    @IBOutlet weak var tableViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cityButton: UIButton!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var categoryButton: UIButton!
    @IBOutlet weak var locationTextField: ZTDropDownTextField!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var topViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var findButton: UIButton!
    @IBOutlet weak var foodTypeButton: UIButton!
    @IBOutlet weak var findBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var adsView: UIView!
    
    // MARK: - Properties
    var isList = false
    var isSearch = false
    var truckers = NSMutableArray()
    
    var selectedTrucker: Trucker?
    var placesClient = GMSPlacesClient()
    var locations = NSMutableArray()
    var location: CLLocation!
    var address: Address!
    var maker = GMSMarker()
    
    var selectedCountry = Country()
    var selectedCity = City()
    var selectedCategory: Category?
    var selectedFoodType: FoodType?
    var popoverContent: FilterTableViewController!
    var adView: FBAdView!
    
    // MARK: - Private vars
    private let maxLoading = 10
    private var openType = "open_now"
    private var openCurrentPage: Int = 1
    private var closeCurrentPage: Int = 1
    private var isShowMore = false
    private var isLoadingMore = false
    private var currentLocation: CLLocation!
    
    /// object to handle location events
    var locationHandler: LocationHandler = {
        var locationHandler = LocationHandler()
        return locationHandler
        }()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: "refreshListOfTruckers:", forControlEvents: UIControlEvents.ValueChanged)
        
        return refreshControl
    }()
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Map view
        mapView.delegate = self
        
        // Margin left
        locationTextField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);

        tableView.tableFooterView = UIView(frame: CGRectMake(0.0, 0.0, CGRectGetWidth(view.frame), 50.0))
        tableView.registerNib(UINib(nibName: "FoodTruckerTableViewCell", bundle: nil),
            forCellReuseIdentifier: foodTruckCellID)
        tableView.registerClass(ShowMoreTableViewCell.self, forCellReuseIdentifier: ShowMoreCellID)
        tableView.addSubview(refreshControl)
        
        locationHandler.delegate = self
        locationHandler.startLocationUpdate()
        
        // Autocomplete location
        locationTextField?.dataSourceDelegate = self
        locationTextField?.animationStyle = ZTDropDownAnimationStyle.Slide
        
        // Button
        cityButton.disable()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadTruckers",
            name: FoodTrackerUpdateTruckerNotification, object: nil)
        
        // Get list of countries from server.
        DataManager.sharedInstance.getCountries { (result, error) -> Void in
            // Do nothing
            
        }
        // Get list of food type from server.
        DataManager.sharedInstance.getFoodTypeList { (result, error) -> Void in
            // Do nothing
        }
        
        // Get previous data
        if segmentedControl.selectedSegmentIndex == 0 {
            truckers = DataManager.sharedInstance.openTruckers
        }
        else {
            truckers = DataManager.sharedInstance.closeTruckers
        }
        
        // Facebook Ads
        adView = FBAdView(placementID: FB_PLACEMENT_ID, adSize: kFBAdSizeHeight50Banner, rootViewController: self)
        adView.delegate = self
        adView.loadAd()
        adsView.addSubview(adView)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Remove border in navigation bar
        navigationController?.navigationBar.setBackgroundImage(UIImage(),
            forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        mapViewHeightConstraint.constant = CGRectGetHeight(view.frame) / 2 - topViewHeightConstraint.constant
        mapView.layoutIfNeeded()
        
        tableView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        let expandImageView = UIImageView(image: UIImage(named: "iconExpand"))
        expandImageView.contentMode = UIViewContentMode.Center
        expandImageView.frame = CGRectMake(CGRectGetWidth(countryButton.frame) - 20.0, 10.0, 10.0, 10.0)
        countryButton.addSubview(expandImageView)
        
        let expandImageView1 = UIImageView(image: UIImage(named: "iconExpand"))
        expandImageView1.contentMode = UIViewContentMode.Center
        expandImageView1.frame = CGRectMake(CGRectGetWidth(cityButton.frame) - 20.0, 10.0, 10.0, 10.0)
        cityButton.addSubview(expandImageView1)
        
        let expandImageView2 = UIImageView(image: UIImage(named: "iconExpand"))
        expandImageView2.contentMode = UIViewContentMode.Center
        expandImageView2.frame = CGRectMake(CGRectGetWidth(categoryButton.frame) - 20.0, 10.0, 10.0, 10.0)
        categoryButton.addSubview(expandImageView2)
        
        let expandImageView3 = UIImageView(image: UIImage(named: "iconExpand"))
        expandImageView3.contentMode = UIViewContentMode.Center
        expandImageView3.frame = CGRectMake(CGRectGetWidth(foodTypeButton.frame) - 20.0, 10.0, 10.0, 10.0)
        foodTypeButton.addSubview(expandImageView3)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Remove border in navigation bar
        navigationController?.navigationBar.setBackgroundImage(nil,
            forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.shadowImage = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PushTruckerDetailVC" {
            if let truckerDetailVC = segue.destinationViewController as? TruckerDetailViewController {
                truckerDetailVC.trucker = selectedTrucker
            }
        }
        else if segue.identifier == "popoverSegue" {
            if let popoverViewController = segue.destinationViewController as? FilterTableViewController {
                popoverViewController.modalPresentationStyle = UIModalPresentationStyle.Popover
                popoverViewController.popoverPresentationController!.delegate = self
            }
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        
        return .None
    }
    
    // MARK: - IBActions
    @IBAction func unwindToHomeVC(segue: UIStoryboardSegue) {
        selectedCountry = Country()
        selectedCity = City()
        selectedCategory = nil
        selectedFoodType = nil
        locationTextField.text = ""
        countryButton.setTitle("Select a Country", forState: UIControlState.Normal)
        cityButton.setTitle("Select a City", forState: UIControlState.Normal)
        categoryButton.setTitle("Business type", forState: UIControlState.Normal)
        foodTypeButton.setTitle("Food Type", forState: UIControlState.Normal)
        cityButton.disable()
    }
    
    @IBAction func segmentedControlDidChangedValue(sender: AnyObject) {
        locationTextField.resignFirstResponder()
        locationTextField.dropDownTableView?.hidden = true
        
        if segmentedControl.selectedSegmentIndex == 0 {
            openType = "open_now"
        }
        else {
            openType = "open_later"
        }
      
        if (DataManager.sharedInstance.openTruckers.count == 0 && openType == "open_now") ||
           (DataManager.sharedInstance.closeTruckers.count == 0 && openType == "open_later") {
            showLoading()
            refreshListOfTruckers(refreshControl)
        }
        else {
            reloadTruckers()
        }
    }
    /*
    @IBAction func searchButtonDidTouched(sender: AnyObject) {
        /*
        locationTextField.resignFirstResponder()
        locationTextField.dropDownTableView?.hidden = true
        
        if isSearch {
            isSearch = false
            currentLocation = location
            searchButton.image = UIImage(named: "btnSearch")
            
            categoryButton.alpha = 1.0
            foodTypeButton.alpha = 1.0
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.topViewHeightConstraint.constant = 44.0
                self.categoryButton.alpha = 0.0
                self.foodTypeButton.alpha = 0.0
                self.view.layoutIfNeeded()
                }, completion: { (Bool) -> Void in
                    self.categoryButton.hidden = true
                    self.foodTypeButton.hidden = true
            })
            
            locationTextField.hidden = true
            cityButton.hidden = true
            countryButton.hidden = true
            segmentedControl.hidden = false
            findBarButtonItem.enabled = false
        }
        else {
            isSearch = true
            currentLocation = nil
            searchButton.image = UIImage(named: "btn_close")
            
            categoryButton.alpha = 0.0
            foodTypeButton.alpha = 0.0
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.topViewHeightConstraint.constant = 74.0
                self.categoryButton.alpha = 1.0
                self.foodTypeButton.alpha = 1.0
                self.view.layoutIfNeeded()
                }, completion: { (Bool) -> Void in
                    self.categoryButton.hidden = false
                    self.foodTypeButton.hidden = false
            })
            
            segmentedControl.hidden = true
            findBarButtonItem.enabled = true
            if isList {
                countryButton.hidden = false
                cityButton.hidden = false
            }
            else {
                locationTextField.hidden = false
            }
        }
        */
    }
    */
    
    @IBAction func menuButtonDidTouched(sender: AnyObject) {
        if self.slidingViewController().currentTopViewPosition == ECSlidingViewControllerTopViewPosition.AnchoredRight {
            self.slidingViewController().resetTopViewAnimated(true)
        }
        else {
            self.slidingViewController().anchorTopViewToRightAnimated(true);
        }
    }
    
    @IBAction func viewModeButtonDidTouched(sender: AnyObject) {
        locationTextField.resignFirstResponder()
        locationTextField.dropDownTableView?.hidden = true
        
        selectedCategory = nil
        selectedFoodType = nil
        categoryButton.setTitle("Business type", forState: UIControlState.Normal)
        foodTypeButton.setTitle("Food Type", forState: UIControlState.Normal)
        
        if (isList) {
            isList = false;
            
            if isSearch {
                locationTextField.hidden = false
                cityButton.hidden = true
                countryButton.hidden = true
                categoryButton.hidden = false
                foodTypeButton.hidden = false
            }
            else {
                categoryButton.hidden = true
                foodTypeButton.hidden = true
            }
            
            UIView.animateWithDuration(0.5, animations: {
                self.viewModeButton.image = UIImage(named: "btnList")
                self.mapViewHeightConstraint.constant = CGRectGetHeight(self.view.frame) / 2 - self.topViewHeightConstraint.constant
                self.mapView.layoutIfNeeded()
                self.tableView.layoutIfNeeded()
            })
        }
        else {
            isList = true;
            
            if isSearch {
                cityButton.hidden = false
                countryButton.hidden = false
                locationTextField.hidden = true
                categoryButton.hidden = false
                foodTypeButton.hidden = false
            }
            else {
                categoryButton.hidden = true
                foodTypeButton.hidden = true
            }
            
            UIView.animateWithDuration(0.5, animations: {
                self.viewModeButton.image = UIImage(named: "btnMap")
                self.mapViewHeightConstraint.constant = 0
                self.mapView.layoutIfNeeded()
                self.tableView.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func filterButtonDidTouched(sender: AnyObject) {
        locationTextField.resignFirstResponder()
        locationTextField.dropDownTableView?.hidden = true
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        popoverContent = storyboard.instantiateViewControllerWithIdentifier("FilterTableVC")
            as! FilterTableViewController
        if sender.tag == 1000 {
            popoverContent.isCountry = true
            popoverContent.countries = DataManager.sharedInstance.countries
        }
        else if sender.tag == 2000 {
            popoverContent.isCity = true
            popoverContent.cities = selectedCountry.cities
        }
        else if sender.tag == 3000 {
            popoverContent.isTruckType = true
            popoverContent.categories = DataManager.sharedInstance.categories
        }
        else {
            popoverContent.foodTypeList = DataManager.sharedInstance.foodTypeList
        }
        popoverContent.delegate = self
        popoverContent.modalPresentationStyle = .Popover
        if let popover = popoverContent.popoverPresentationController {
            let viewForSource = sender as! UIView
            popover.sourceView = viewForSource
            
            // the position of the popover where it's showed
            popover.sourceRect = viewForSource.bounds
            
            // the size you want to display
            popoverContent.preferredContentSize = CGSizeMake(170, 176)
            popover.delegate = self
        }
        self.presentViewController(popoverContent, animated: true, completion: nil)
    }
    
    @IBAction func locationTextFieldDidChangedValue(sender: AnyObject) {
        let textField = sender as! UITextField
        if textField.text?.characters.count == 0 {
            currentLocation = nil;
        }
    }
    
    @IBAction func findButtonDidTouched(sender: AnyObject) {
        searchTruckers()
    }
    
    // MARK: - Private methods
    private func setupGMSMarker() {
        maker.title = "My Location";
        maker.map   = mapView!;
        maker.icon  = UIImage(named:"truckerIcon")
    }
    
    private func autocompleteAddress(address: String) {
        let filter = GMSAutocompleteFilter()
        filter.type = GMSPlacesAutocompleteTypeFilter.Address
        placesClient.autocompleteQuery(address, bounds: nil, filter: filter, callback: { (results, error: NSError?) -> Void in
            
            self.locationTextField.dropDownTableView?.hidden = false
            if let error = error {
                print("Autocomplete error \(error)")
                if self.locationTextField?.dropDownTableView != nil {
                    self.locations.removeAllObjects()
                    self.locationTextField?.dropDownTableView.reloadData()
                }
                
            } else {
                if results != nil {
                    self.locations = NSMutableArray(array: results!)
                    if self.locations.count < 4 {
                        self.locationTextField?.dropDownTableViewHeight = CGFloat(self.locations.count) * (self.locationTextField?.rowHeight)! ?? 0
                    } else {
                        self.locationTextField?.dropDownTableViewHeight = 200
                    }
                    self.locationTextField?.dropDownTableView.reloadData()
                } else {
                    self.locations.removeAllObjects()
                    self.locationTextField?.dropDownTableView.reloadData()
                }
            }
        })
    }

    @IBAction func textFieldDidChangedValue(sender: AnyObject) {
        if let string = locationTextField!.text {
            autocompleteAddress(string)
        }
    }
    
    private func zoomMaptoLocation(location: CLLocation) {
        let cameraPosition = GMSCameraPosition.cameraWithLatitude(location.coordinate.latitude,
            longitude: location.coordinate.longitude, zoom: 14);
        mapView.camera =  cameraPosition
    }
    
    private func moveMarkerinMap(location: CLLocation!, address: String) {
        if location != nil {
            maker.snippet  = address ?? ""
            maker.position = location.coordinate
            zoomMaptoLocation(location)
        }
    }
    
    private func searchTruckers() {
        var lat = ""
        var lng = ""
        if currentLocation != nil {
            lat = String(currentLocation.coordinate.latitude)
            lng = String(currentLocation.coordinate.longitude)
        }
        
        var categoryType = ""
        if let category = selectedCategory {
            categoryType = category.categoryType
        }
        var foodTypeId = ""
        if let foodType = selectedFoodType {
            foodTypeId = String(foodType.foodTypeId)
        }
        
        DataManager.sharedInstance.getTruckersByLatitude(lat, longtitude: lng, city: selectedCity.cityName, truckType: categoryType, openType: "", foodType: foodTypeId, page: 0, perPage: 0) { (result, error) -> Void in
            if error == nil {
                self.performSegueWithIdentifier("PushSearchResultVC", sender: result)
            }
        }
    }
    
    func reloadTruckers() {
        if segmentedControl.selectedSegmentIndex == 0 {
            truckers = DataManager.sharedInstance.openTruckers
        }
        else {
            truckers = DataManager.sharedInstance.closeTruckers
        }
        
        // Draw list of trucker on map
        self.mapView.clear()
        for trucker in self.truckers {
            self.mapView.configureMarkersForTrucker(trucker as! Trucker)
        }
        
        tableView.reloadData()
    }
    
    func refreshListOfTruckers(refreshControl: UIRefreshControl) {
        var lat = ""
        var lng = ""
        if currentLocation != nil {
            lat = String(currentLocation.coordinate.latitude)
            lng = String(currentLocation.coordinate.longitude)
        }
        
        DataManager.sharedInstance.getTruckersByLatitude(lat, longtitude: lng, city: "", truckType: "",
            openType: openType, foodType: "", page: 1, perPage: maxLoading) { (result, error) -> Void in

                self.hideLoading()
                if error != nil {
                    self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
                }
                else {
                    let currentTruckers = result as! [Trucker]
                    self.isShowMore = currentTruckers.count == self.maxLoading
                    
                    self.isLoadingMore = false
                    
                    // Reset current page
                    if self.openType == "open_now" {
                        self.openCurrentPage = 1
                    }
                    else {
                        self.closeCurrentPage = 1
                    }
                    self.reloadTruckers()
                    
                    refreshControl.endRefreshing()
                }
        }
    }
    
    private func loadingMoreListOfTruckers() {
        var lat = ""
        var lng = ""
        if currentLocation != nil {
            lat = String(currentLocation.coordinate.latitude)
            lng = String(currentLocation.coordinate.longitude)
        }
        
        if segmentedControl.selectedSegmentIndex == 0 {
            openCurrentPage += 1
        }
        else {
            closeCurrentPage += 1
        }
        let currentPage = segmentedControl.selectedSegmentIndex == 0 ? openCurrentPage : closeCurrentPage
        DataManager.sharedInstance.getTruckersByLatitude(lat, longtitude: lng, city: "", truckType: "",
            openType: openType, foodType: "", page: currentPage, perPage: maxLoading) { (result, error) -> Void in
            if error != nil {
                self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
            }
            else {
                let currentTruckers = result as! [Trucker]
                self.isShowMore = currentTruckers.count == self.maxLoading
                
                self.isLoadingMore = false
                self.reloadTruckers()
            }
        }
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return truckers.count > 0 ? truckers.count + 1 : 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.row == truckers.count {
            // Initialize show more cell & display it
            var cell: ShowMoreTableViewCell? = tableView.dequeueReusableCellWithIdentifier(ShowMoreCellID)  as? ShowMoreTableViewCell
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: ShowMoreCellID) as? ShowMoreTableViewCell
            }
            
            cell!.separatorInset = UIEdgeInsetsMake(0, 160, 0, 160);
            if !(cell!.activityIndicator.isAnimating()) && isShowMore {
                cell!.activityIndicator.startAnimating()
            }
            
            return cell!
        }
        
        // Start load more neighbors
        if (indexPath.row == truckers.count - 1 && isShowMore && !isLoadingMore) {
            isLoadingMore = true;
                
            // Load more neighbor list data
            loadingMoreListOfTruckers()
        }
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier(foodTruckCellID, forIndexPath: indexPath) as! FoodTruckerTableViewCell
        let trucker = truckers[indexPath.row] as! Trucker
        
        cell.nameLabel.text = trucker.truckername
        cell.addressLabel.text = trucker.truckerDecs
        cell.favoriteButton.selected = trucker.liked
        cell.rateButton.setTitle("\(trucker.averageRate)", forState: UIControlState.Normal)
        if trucker.truckerImages.count > 0 {
            let truckerImageUrl = trucker.truckerImages[0].image?.large
            cell.truckImageView.kf_setImageWithURL(NSURL(string: truckerImageUrl!)!)
        }
        else {
            cell.truckImageView.image = UIImage(named: "truckerPlaceholder")
        }
        
        return cell
    }
}

// MARK : - UITableViewDelegate

extension HomeViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        selectedTrucker = truckers[indexPath.row] as? Trucker
        
        performSegueWithIdentifier("PushTruckerDetailVC", sender: nil)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == truckers.count {
            return isShowMore ? 44.0 : 0
        }
        
        return 75.0
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView .dequeueReusableCellWithIdentifier("HeaderCellID")
        let label = headerCell?.viewWithTag(1000) as! UILabel
        label.text = truckers.count == 1 ? "\(truckers.count) Place" : "\(truckers.count) Places"
        
        return headerCell;
    }
}

extension HomeViewController: LocationHandlerDelegate {
    func locationDetected(location: CLLocation) {
    
        print("%@", location)
        self.location = location
        currentLocation = location
        zoomMaptoLocation(location)
        
        var lat = ""
        var lng = ""
        if currentLocation != nil {
            lat = String(currentLocation.coordinate.latitude)
            lng = String(currentLocation.coordinate.longitude)
        }
        
        // Don't display loading indicator if having previous data
        if truckers.count == 0 {
            showLoading()
        }
        DataManager.sharedInstance.getTruckersByLatitude(lat, longtitude: lng, city: "", truckType: "",
            openType: openType, foodType: "", page: 1, perPage: maxLoading) { (result, error) -> Void in
            if error != nil {
                self.hideLoading()
                self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
            }
            else {
                let currentTruckers = result as! [Trucker]
                self.isShowMore = currentTruckers.count == self.maxLoading
                
                self.hideLoading()
                self.reloadTruckers()
            }
        }
    }
    
    func locationDiscoveryFailed(error: NSError) {
        print("Location failed with error: %@", error)
    }
}

extension HomeViewController: ZTDropDownTextFieldDataSourceDelegate {
    func dropDownTextField(dropDownTextField: ZTDropDownTextField,
        numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    func dropDownTextField(dropDownTextField: ZTDropDownTextField, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = dropDownTextField.dropDownTableView?.dequeueReusableCellWithIdentifier("Cell")
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: "Cell")
        }
        cell?.textLabel?.font = UIFont.openSansRegular(size: 12)
        cell?.backgroundColor = UIColor.clearColor()
        let object = locations.objectAtIndex(indexPath.row) as! GMSAutocompletePrediction
        cell!.textLabel!.text = object.attributedFullText.string

        return cell!
    }
    
    func dropDownTextField(dropdownTextField: ZTDropDownTextField, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let object = locations.objectAtIndex(indexPath.row) as! GMSAutocompletePrediction
        placesClient.lookUpPlaceID(object.placeID, callback: {
            (place, error) -> Void in
            if let gmsPlace: GMSPlace = place {
                self.currentLocation = CLLocation(latitude:gmsPlace.coordinate.latitude,
                    longitude: gmsPlace.coordinate.longitude)
            }
        })
        locationTextField?.text = object.attributedFullText.string
    }
}

// MARK: - UIPopoverControllerDelegate
extension HomeViewController: UIPopoverPresentationControllerDelegate {
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        //do som stuff from the popover
    }
}

extension HomeViewController: FilterTableViewControllerDelegate {
    func filterControllerDidSelectedCity(city: City) {
        selectedCity = city
        cityButton.setTitle(city.cityName, forState: UIControlState.Normal)

        popoverContent.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func filterControllerDidSelectedCountry(country: Country) {
        selectedCountry = country
        cityButton.enable()
        
        DataManager.sharedInstance.getCitiesByCountryId(self.selectedCountry.countryId,
            completionHandler: { (result, error) -> Void in
                // Do nothing
        })
        countryButton.setTitle(country.countryName, forState: UIControlState.Normal)
        cityButton.setTitle("Select a City", forState: UIControlState.Normal)
        
        popoverContent.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func filterControllerDidSelectedCategory(category: Category) {
        selectedCategory = category
        
        if category.categoryName == "None" {
            categoryButton.setTitle("Business type", forState: UIControlState.Normal)
        }
        else {
            categoryButton.setTitle(category.categoryName, forState: UIControlState.Normal)
        }
        
        popoverContent.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func filterControllerDidSelectedFoodType(foodType: FoodType) {
        selectedFoodType = foodType
        
        if foodType.foodTypeName == "None" {
            foodTypeButton.setTitle("Food Type", forState: UIControlState.Normal)
        }
        else {
            foodTypeButton.setTitle(foodType.foodTypeName, forState: UIControlState.Normal)
        }
        
        popoverContent.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension HomeViewController: GMSMapViewDelegate {
    func mapView(mapView: GMSMapView!, didTapInfoWindowOfMarker marker: GMSMarker!) {
        print(marker)
        selectedTrucker = marker.userData as? Trucker
        
        performSegueWithIdentifier("PushTruckerDetailVC", sender: nil)
    }
}

extension HomeViewController: UITextFieldDelegate {
    func textFieldShouldClear(textField: UITextField) -> Bool {
        currentLocation = nil
        
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        guard currentLocation != nil else {
            showOkeyMessage(ERROR_TITLE, message: "Please select a location!")
            return true
        }
        
        searchTruckers()
        
        return true
    }
}

extension HomeViewController: FBAdViewDelegate {
    func adViewDidLoad(adView: FBAdView) {
        print("adViewDidLoad")
        adsView.hidden = false
    }
    
    func adView(adView: FBAdView, didFailWithError error: NSError) {
        print("didFailWithError")
        adsView.hidden = true
    }
}