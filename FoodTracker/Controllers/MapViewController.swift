//
//  MapViewController.swift
//  FoodTracker
//
//  Created by An Phan on 11/25/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

enum TravelModes: Int {
    case driving
    case walking
    case bicycling
}

class MapViewController: UIViewController {
    
    var trucker = Trucker()
    var mapTasks = GoogleMapManager()
    var originMarker: GMSMarker!
    var routePolyline: GMSPolyline!
    
    @IBOutlet var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.settings.compassButton = true

        // Do any additional setup after loading the view.
        let original = "\(DataManager.sharedInstance.currentLocation.coordinate.latitude)" + "," + "\(DataManager.sharedInstance.currentLocation.coordinate.longitude)"
        let destination = (trucker.address.latitude) + "," + (trucker.address.longitude)
        mapTasks.getDirections(original, destination: destination, waypoints: nil, travelMode: TravelModes.driving,
            completionHandler: { (status, success) -> Void in
            if success {
                self.configureMapAndMarkersForRoute()
                self.drawRoute()
            }
            else {
                print(status)
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureMapAndMarkersForRoute() {
        mapView.configureMarkersForTrucker(trucker)
        
        mapView.camera = GMSCameraPosition.cameraWithTarget(mapTasks.originCoordinate, zoom: 14.0)
        
        originMarker = GMSMarker(position: self.mapTasks.originCoordinate)
        originMarker.map = mapView
        originMarker.icon = GMSMarker.markerImageWithColor(UIColor.greenColor())
        originMarker.title = self.mapTasks.originAddress
    }
    
    
    func drawRoute() {
        let route = mapTasks.overviewPolyline["points"] as? String
        
        let path: GMSPath = GMSPath(fromEncodedPath: route)
        routePolyline = GMSPolyline(path: path)
        routePolyline.strokeWidth = 5
        routePolyline.map = mapView
    }
}
