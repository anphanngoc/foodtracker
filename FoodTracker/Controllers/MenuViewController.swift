//
//  MenuTableViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/11/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import Kingfisher

class MenuViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var menuTableView: UITableView!
    
    // MARK: - Properties
    let menuTitles = ["HOME", "FOLLOWING", "ABOUT", "SIGN OUT"];
    let menuImages = ["iconHome", "iconFavorite", "iconAbout", "iconLogout"]
    
    // MARK: - Private properties
    private var selectedMenuItem : Int = 0
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.menuTableView.tableFooterView = UIView()
        self.avatarImageView.layer.cornerRadius = 50.0

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadMenu", name: FoodTrackerUpdateUserNotification, object: nil)
        
        reloadMenu()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

    // MARK: - Actions
    @IBAction func gearButtonDidTouched(sender: AnyObject) {
        // Do nothing
    }
    
    // MARK: - Helper methods
    private func showSignOutAlert() {
        let actionSheet = UIAlertController(title: "Are you sure you want to sign out?", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let logOutAction = UIAlertAction(title: "Sign out", style: UIAlertActionStyle.Destructive) { (alertAction) -> Void in

            self.showLoading()
            DataManager.sharedInstance.logoutUserWithCompletionHandler({ (result, error) -> Void in
                if error != nil {
                    self.hideLoading()
                    self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
                }
                else {
                    self.hideLoading()
                    AppDelegate.sharedAppDelegate().setWelCome()
                }
            })
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (alertAction) -> Void in
            
        }
        
        actionSheet.addAction(logOutAction)
        actionSheet.addAction(cancelAction)
        
        let topVC = UIApplication.topViewController()
        topVC!.presentViewController(actionSheet, animated: true, completion: nil)
    }
    
    func setTopViewController(viewController: UIViewController) {
        self.slidingViewController().topViewController = viewController
        viewController.view.addGestureRecognizer(self.slidingViewController().panGesture)
        self.slidingViewController().resetTopViewAnimated(true)
    }
    
    func reloadMenu() {
        let user = DataManager.sharedInstance.loggedUser
        nameLabel.text = user.userName
        avatarImageView.kf_setImageWithURL(NSURL(string: user.avatar.large)!,
            placeholderImage: UIImage(named: "avatarDefault"))
    }
}

// MARK: - Table view data source
extension MenuViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuTitles.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Configure the cell...
        var cell = tableView.dequeueReusableCellWithIdentifier("menuCell")
        
        if (cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "menuCell")
            cell!.backgroundColor = UIColor.clearColor()
            cell!.textLabel?.textColor = UIColor.darkGrayColor()
            let selectedBackgroundView = UIView(frame: CGRectMake(0, 0, cell!.frame.size.width, cell!.frame.size.height))
            selectedBackgroundView.backgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.2)
            cell!.selectedBackgroundView = selectedBackgroundView
        }
        
        cell!.textLabel?.text = self.menuTitles[indexPath.row]

        return cell!
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.textLabel?.font = UIFont.openSansSemibold(size: 12)
        if indexPath.row == selectedMenuItem {
            cell.textLabel?.textColor = UIColor.mainColor()
            let imageName = self.menuImages[indexPath.row] + "Selected"
            cell.imageView?.image = UIImage(named: imageName)
        }
        else {
            cell.textLabel?.textColor = UIColor.whiteColor()
            cell.imageView?.image = UIImage(named: self.menuImages[indexPath.row])
        }
    }
}

extension MenuViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if (indexPath.row == selectedMenuItem && indexPath.row != 3) {
            return
        }

        //Present new view controller
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main",bundle: nil)
        var destViewController : UIViewController
        switch (indexPath.row) {
        case 0:
            selectedMenuItem = indexPath.row
            slidingViewController().resetTopViewAnimated(true)
            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("HomeNC")
            setTopViewController(destViewController)
            break
        case 1:
            selectedMenuItem = indexPath.row
            slidingViewController().resetTopViewAnimated(true)
            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("FavoriteNC")
            setTopViewController(destViewController)
            break
        case 2:
            selectedMenuItem = indexPath.row
            slidingViewController().resetTopViewAnimated(true)
            destViewController = mainStoryboard.instantiateViewControllerWithIdentifier("AboutNC")
            setTopViewController(destViewController)
            break
        default:
            showSignOutAlert()
            break
        }
        tableView.reloadData()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50.0
    }
}
