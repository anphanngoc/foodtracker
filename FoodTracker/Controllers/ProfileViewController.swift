//
//  ProfileViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/14/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import Kingfisher
import ObjectMapper

class ProfileViewController: UIViewController {

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var changePwdButton: UIButton!
    @IBOutlet weak var rightBarButtonItem: UIBarButtonItem!
    
    var user = DataManager.sharedInstance.loggedUser
    var userName = ""
    var email = ""
    var phone = ""
    var gender = "male"
    var imagePickerController = UIImagePickerController()
    
    var countryCode = ("","")
    
    var isEdit = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        avatarImageView.layer.cornerRadius = 45.0
        tableView.tableFooterView = UIView()
        changePwdButton.titleLabel?.font = UIFont.openSansRegular(size: 14)
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            var tmpFrame = topView.frame
            tmpFrame.size.height = 150.0
            topView.frame = tmpFrame
        }
        tableView.tableHeaderView = topView
        
        // Default value
        userName = user.userName
        email = user.email
        gender = user.gender
        
        countryCode = splitCountryCodeinPhoneNumber(user.phone)
        if countryCode.0 == "" {
            var currentCode = "+61"
            let contriesDict = NSArray.readArraybyFileJSON("countries")!
            let countries = Mapper<CountryCode>().mapArray(contriesDict)
            let isoCode = getIsoCountryCode()
            for countryObject: CountryCode in countries! {
                if isoCode.uppercaseString == countryObject.code {
                    currentCode = countryObject.dialCode
                    break
                }
            }
            
            countryCode.0 = currentCode
        }
        
        avatarImageView.kf_setImageWithURL(NSURL(string: user.avatar.large)!,
            placeholderImage: UIImage(named: "avatarDefault"))
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Remove border in navigation bar
        navigationController?.navigationBar.setBackgroundImage(UIImage(),
            forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Remove border in navigation bar
        navigationController?.navigationBar.setBackgroundImage(nil,
            forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.shadowImage = nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func backButtonDidTouched(sender: AnyObject) {
        dismissViewControllerAnimated(true) { () -> Void in
            print("Dismissed")
        }
    }
    
    @IBAction func unwindToProfileVC(segue: UIStoryboardSegue) {
    }

    @IBAction func rightBarButtonDidTouched(sender: AnyObject) {
        if isEdit {
            isEdit = false
            tableView.userInteractionEnabled = false
            avatarImageView.userInteractionEnabled = false
            rightBarButtonItem.image = UIImage(named: "btnPen")
            title = "Account Information"
            
            phone = "(\(countryCode.0))" + countryCode.1
            
            // Send request update user info
            updateAccountInfo()
        }
        else {
            isEdit = true
            rightBarButtonItem.image = UIImage(named: "btnDone")
            tableView.userInteractionEnabled = true
            avatarImageView.userInteractionEnabled = true
            title = "Edit Account"
        }
    }
    
    @IBAction func avatarImageDidTouched(sender: AnyObject) {
        let actionSheet:UIAlertController = UIAlertController(title:CAMERA_TITLE, message:CAMERA_DESCRIPTION, preferredStyle:.ActionSheet)
        
        let cancelAction = UIAlertAction(title:ALERT_CANCEL, style:.Cancel,handler: {
            action -> Void in
        })
        actionSheet.addAction(cancelAction)
        
        let photoAction = UIAlertAction(title:CAMERA_TAKE_A_PHOTO, style:.Default, handler: {
            action -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera) {
                self.imagePickerController.sourceType    = UIImagePickerControllerSourceType.Camera
                self.imagePickerController.allowsEditing = true
                self.presentViewController(self.imagePickerController, animated: true, completion: { () in
                    UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
                })
            }
        })
        actionSheet.addAction(photoAction)
        
        let cameraRollAction = UIAlertAction(title:CAMERA_CAMERA_ROLL, style:.Default, handler:{
            action -> Void in
            self.imagePickerController.sourceType    = UIImagePickerControllerSourceType.PhotoLibrary
            self.imagePickerController.allowsEditing = true
            self.presentViewController(self.imagePickerController, animated: true, completion: { () in
                UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
            })
        })
        actionSheet.addAction(cameraRollAction)
        self.imagePickerController.delegate = self
        self.presentViewController(actionSheet, animated: true, completion:nil)
    }
    
    // MARK: - Actions
    func textFieldDidChangedValue(textField: UITextField) {
        if textField.tag == 100 {
            userName = textField.text ?? ""
        }
        else if textField.tag == 200 {
            email = textField.text ?? ""
        }
        else if textField.tag == 300 {
            countryCode.1 = textField.text ?? ""
        }
    }
    
    // MARK: - Helpers 
    func updateAccountInfo() {
        guard !email.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "Email is blank!")
            return
        }
        
        guard email.isValidEmail() else {
            showOkeyMessage(ERROR_TITLE, message: "Please enter a valid email!")
            return
        }
        
        guard !userName.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "Name is blank!")
            return
        }
        
        showLoading()
        DataManager.sharedInstance.updateUserInfoWithName(userName, email: email, phone: phone, gender: gender,
            completionHandler: { (result, error) -> Void in
                if error != nil {
                    self.hideLoading()
                    self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
                }
                else {
                    self.hideLoading()
                    self.showOkeyMessage(FOOD_TRUCKER, message: "Update successfully!")
                }
        })
    }
}

// MARK: - UITableViewDataSource
extension ProfileViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cellIdentifier = "InputCellID"
        if indexPath.row == 2 {
            cellIdentifier = "InputPhoneCellID"
        }
        else if (indexPath.row == 3) {
            cellIdentifier = "GenderCellID"
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier,
            forIndexPath: indexPath) as! InputTableViewCell
        
        // Add target action for text fields
        cell.textField?.addTarget(self, action: "textFieldDidChangedValue:", forControlEvents: UIControlEvents.EditingChanged)
        
        if (indexPath.row == 0) {
            cell.textField?.tag = 100
            let name = NSMutableAttributedString(string: "Name *")
            let nameRange: NSRange = "Name *".rangeOfString("*")
            name.addAttribute(NSForegroundColorAttributeName, value: UIColor.rgbColor(red: 218.0, green: 80.0, blue: 14.0), range: nameRange)
            cell.titleLabel?.attributedText = name
            cell.textField?.placeholder = "Enter your name"
            cell.textField?.text = user?.userName
        }
        else if (indexPath.row == 1) {
            cell.textField?.tag = 200
            let name = NSMutableAttributedString(string: "Email *")
            let nameRange: NSRange = "Email *".rangeOfString("*")
            name.addAttribute(NSForegroundColorAttributeName, value: UIColor.rgbColor(red: 218.0, green: 80.0, blue: 14.0), range: nameRange)
            cell.titleLabel?.attributedText = name
            cell.textField?.placeholder = "Enter your email"
            cell.textField?.autocapitalizationType = UITextAutocapitalizationType.None;
            cell.textField?.autocorrectionType = UITextAutocorrectionType.No;
            cell.textField?.text = user?.email
        }
        else if (indexPath.row == 2) {
            cell.textField?.tag = 300
            cell.textField?.delegate = self
            cell.textField?.keyboardType = UIKeyboardType.PhonePad
            cell.titleLabel?.text = "Phone No."
            cell.textField?.placeholder = "Enter your phone"
            cell.textField?.text = countryCode.1
            
            cell.dialCodeTextField.delegate = self;
            cell.dialCodeTextField.text = "(\(countryCode.0))"
        }
        else if (indexPath.row == 3) {
            cell.delegate = self
            if user?.gender == "male" {
                cell.genderSwitch.on = false
            }
            else {
                cell.genderSwitch.on = true
            }
        }
        
        return cell
    }
}

// MARK: - UIImagePickerControllerDelegate
extension ProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        let image = info[UIImagePickerControllerEditedImage] as? UIImage
        let imageCropVC = RSKImageCropViewController(image: image)
        imageCropVC.delegate = self
        
        self.navigationController?.pushViewController(imageCropVC, animated: false)
        dismissViewControllerAnimated(false) { () -> Void in
            UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        }
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        self.imagePickerController .dismissViewControllerAnimated(true, completion: { () in
            UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        })
    }
}

// MARK: - RSKImageCropViewControllerDelegate
extension ProfileViewController: RSKImageCropViewControllerDelegate {
    
    // Did canceled
    func imageCropViewControllerDidCancelCrop(controller: RSKImageCropViewController!) {
        navigationController?.popToRootViewControllerAnimated(true)
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
    }
    
    // Did cropped image
    func imageCropViewController(controller: RSKImageCropViewController!, didCropImage
        croppedImage: UIImage!, usingCropRect cropRect: CGRect) {
            navigationController?.popToRootViewControllerAnimated(true)
            UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
            
            avatarImageView.image = croppedImage
            
            avatarImageView.rn_activityView.color = UIColor.clearColor()
            avatarImageView.showActivityView()
            DataManager.sharedInstance.uploadUserAvatar(croppedImage) { (result, error) -> Void in
                if error != nil {
                    self.avatarImageView.hideActivityView()
                    self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
                }
                else {
                    self.avatarImageView.hideActivityView()
                    // TODO: Will implement later
                }
            }
    }
}

// MARK: - UINavigationControllerDelegate
extension ProfileViewController: UINavigationControllerDelegate {
    func navigationController(navigationController: UINavigationController,
        willShowViewController viewController: UIViewController, animated: Bool) {
            
            // Status bar style
            UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
            
            // Navigation bar style
            navigationController.navigationBar.tintColor = UIColor.whiteColor()
            navigationController.navigationBar.barTintColor = .mainColor()
            navigationController.navigationBar.translucent = false
            navigationController.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSFontAttributeName: UIFont.openSansRegular(size: 14)]
    }
}

// MARK: - InputTableViewCellDelegate
extension ProfileViewController: InputTableViewCellDelegate {
    func inputCellGenderSwitchDidChangedValue(sender: AnyObject) {
        let genderSwitch = sender as? SevenSwitch
        if genderSwitch?.isOn() == true {
            gender = "female"
        }
        else {
            gender = "male"
        }
    }
}

// MARK: - UITextFieldDelegate
extension ProfileViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        if textField.tag == 1000 {  // Dial code text field
            let countryPickerVC = CountryPickerViewController()
            countryPickerVC.delegate = self
            countryPickerVC.dialCode = countryCode.0
            countryPickerVC.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
            presentViewController(countryPickerVC, animated: false, completion: nil)
            
            return false
        }
        
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange,
        replacementString string: String) -> Bool {
        if textField.tag == 300 {
            if range.location == 0 && string == "0" {
                return false
            }
            else {
                return string.isPhoneNumber
            }
        }
            
        return true
    }
}

// MARK: - CountryPickerViewControllerDelegate
extension ProfileViewController: CountryPickerViewControllerDelegate {
    func countryPickerDidCancelled() {
        // Do nothing
    }
    
    func countryPickerDidSelectedCountry(country: CountryCode) {
        countryCode.0 = country.dialCode
        let indexPath = NSIndexPath(forRow: 2, inSection: 0)
        let inputCell = tableView.cellForRowAtIndexPath(indexPath) as! InputTableViewCell
        inputCell.dialCodeTextField.text = "(\(country.dialCode))"
    }
}