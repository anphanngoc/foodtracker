//
//  ResetPasswordViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/31/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func sendButtonDidTouched(sender: AnyObject) {
        emailTextField.resignFirstResponder()
        
        let email = emailTextField.text?.trim() ?? ""
        
        guard !email.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "Email is blank!")
            return
        }
        guard email.isValidEmail() else {
            showOkeyMessage(ERROR_TITLE, message: "Please enter a valid email!")
            return
        }
        
        showLoading()
        DataManager.sharedInstance.sendRequestResetPasswordToEmail(email) { (result, error) -> Void in
            if error != nil {
                self.hideLoading()
                self.showOkeyMessage(FOOD_TRUCKER, message: Utilities.getErrorMessage(error))
            }
            else {
                self.hideLoading()
                self.showOkeyMessage(FOOD_TRUCKER, message: "Reset password email was sent to you!")
            }
        }
    }
}

extension ResetPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        sendButtonDidTouched(UIButton())
        
        return true
    }
}
