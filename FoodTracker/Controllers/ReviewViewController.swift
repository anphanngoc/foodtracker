//
//  ReviewViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/31/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import Cosmos

class ReviewViewController: UIViewController {

    @IBOutlet weak var reviewTextField: UITextView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var doneBarButtonItem: UIBarButtonItem!
    
    var rating = 0
    var reviewContent = ""
    
    var trucker: Trucker!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        reviewTextField.layer.borderColor = UIColor.rgbColor(red: 217.0, green: 217.0, blue: 217.0).CGColor
        reviewTextField.layer.borderWidth = 1.0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions

    @IBAction func doneButtonDidTouched(sender: AnyObject) {
        reviewTextField.resignFirstResponder()
        rating = Int(ratingView.rating)
        
        guard rating > 0 && !reviewContent.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "You didn't select a rating value and enter your review!")
            return
        }
        
        showLoading()
        DataManager.sharedInstance.reviewTrucker(trucker.truckerId, rate: rating, reviewContent: reviewContent) { (result, error) -> Void in
            if error != nil {
                self.hideLoading()
                self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
            }
            else {
                self.hideLoading()
                self.navigationController?.popViewControllerAnimated(true)
            }
        }
    }
    
    func textViewDidChange(textView: UITextView) {
        reviewContent = textView.text
    }
}

extension ReviewViewController: UITextViewDelegate {
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange,
        replacementText text: String) -> Bool {
            if text == "\n" {
                textView.resignFirstResponder()
                doneButtonDidTouched(doneBarButtonItem)
                return false
            }
            
            return true
    }
}
