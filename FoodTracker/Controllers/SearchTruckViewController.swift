//
//  FollowingTruckViewController.swift
//  FoodTracker
//
//  Created by An Phan on 12/12/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

class SearchTruckViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var cityButton: UIButton!
    @IBOutlet weak var truckTypeButton: UIButton!
    @IBOutlet weak var foodTypeButton: UIButton!
    @IBOutlet weak var adsView: UIView!
    
    var popoverContent: FilterTableViewController!
    
    var truckers = [Trucker]()
    var selectedTrucker: Trucker?
    var selectedCountry = Country()
    var selectedCity = City()
    var selectedCategory: Category?
    var selectedFoodType: FoodType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var originX: CGFloat = 130.0
        if DeviceType.IS_IPHONE_6 {
            originX = 155.0
        }
        else if DeviceType.IS_IPHONE_6P {
            originX = 175.0
        }
        let tmpFrame = CGRectMake(originX, 10.0, 10.0, 10.0)
        let expandImageView = UIImageView(image: UIImage(named: "iconExpand"))
        expandImageView.contentMode = UIViewContentMode.Center
        expandImageView.frame = tmpFrame
        countryButton.addSubview(expandImageView)
        
        let expandImageView1 = UIImageView(image: UIImage(named: "iconExpand"))
        expandImageView1.contentMode = UIViewContentMode.Center
        expandImageView1.frame = tmpFrame
        cityButton.addSubview(expandImageView1)
        
        let expandImageView2 = UIImageView(image: UIImage(named: "iconExpand"))
        expandImageView2.contentMode = UIViewContentMode.Center
        expandImageView2.frame = tmpFrame
        truckTypeButton.addSubview(expandImageView2)
        
        let expandImageView3 = UIImageView(image: UIImage(named: "iconExpand"))
        expandImageView3.contentMode = UIViewContentMode.Center
        expandImageView3.frame = tmpFrame
        foodTypeButton.addSubview(expandImageView3)
        
        tableView.tableFooterView = UIView(frame: CGRectMake(0.0, 0.0, CGRectGetWidth(view.frame), 50.0))
        tableView.registerNib(UINib(nibName: "FoodTruckerTableViewCell", bundle: nil),
            forCellReuseIdentifier: foodTruckCellID)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadTruckers:",
            name: FoodTrackerUpdateTruckerNotification, object: nil)
        
        // Set default value for the country and city
        if DataManager.sharedInstance.countries.count > 0 {
            selectedCountry = DataManager.sharedInstance.countries[0]
            countryButton.setTitle(selectedCountry.countryName, forState: UIControlState.Normal)
        }
        let cities = DataManager.sharedInstance.getLocalCitiesByCountry(selectedCountry.countryId)
        if cities.count > 0 {
            selectedCity = cities[0]
            cityButton.setTitle(selectedCity.cityName, forState: UIControlState.Normal)
            searchTruckers()
        }
        
        // Facebook Ads
        let adView = FBAdView(placementID: FB_PLACEMENT_ID, adSize: kFBAdSizeHeight50Banner, rootViewController: self)
        adView.delegate = self
        adView.loadAd()
        adsView.addSubview(adView)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Remove border in navigation bar
        navigationController?.navigationBar.setBackgroundImage(UIImage(),
            forBarMetrics: UIBarMetrics.Default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PushTruckerDetailVC" {
            if let truckerDetailVC = segue.destinationViewController as? TruckerDetailViewController {
                truckerDetailVC.trucker = selectedTrucker
            }
        }
    }
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        
        return .None
    }
    
    @IBAction func filterButtonDidTouched(sender: AnyObject) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        popoverContent = storyboard.instantiateViewControllerWithIdentifier("FilterTableVC")
            as! FilterTableViewController
        if sender.tag == 1000 {
            popoverContent.isCountry = true
            popoverContent.countries = DataManager.sharedInstance.countries
        }
        else if sender.tag == 2000 {
            popoverContent.isCity = true
            popoverContent.cities = selectedCountry.cities
        }
        else if sender.tag == 3000 {
            popoverContent.isTruckType = true
            popoverContent.categories = DataManager.sharedInstance.categories
        }
        else {
            popoverContent.foodTypeList = DataManager.sharedInstance.foodTypeList
        }
        popoverContent.delegate = self
        popoverContent.modalPresentationStyle = .Popover
        if let popover = popoverContent.popoverPresentationController {
            let viewForSource = sender as! UIView
            popover.sourceView = viewForSource
            
            // the position of the popover where it's showed
            popover.sourceRect = viewForSource.bounds
            
            // the size you want to display
            popoverContent.preferredContentSize = CGSizeMake(170, 176)
            popover.delegate = self
        }
        self.presentViewController(popoverContent, animated: true, completion: nil)
    }
    
    private func searchTruckers() {
        var categoryType = ""
        if let category = selectedCategory {
            categoryType = category.categoryType
        }
        var foodTypeId = ""
        if let foodType = selectedFoodType {
            foodTypeId = String(foodType.foodTypeId)
        }
        
        showLoading()
        DataManager.sharedInstance.getTruckersByLatitude("", longtitude: "", city: selectedCity.cityName,
            truckType: categoryType, openType: "", foodType: foodTypeId, page: 0, perPage: 0) { (result, error) -> Void in
                self.hideLoading()
            if error == nil {
                self.truckers = result as! [Trucker]
                self.tableView.reloadData()
            }
        }
    }
    
    func reloadTruckers(notification: NSNotification) {
        let liked = notification.userInfo?["liked"] as? Bool ?? false
        let truckerId = notification.userInfo?["truckerId"] as? Int ?? 0
        // Update list of truckers
        for trucker in truckers {
            if truckerId == trucker.truckerId {
                trucker.liked = liked
                break;
            }
        }
        
        tableView.reloadData()
    }
}

extension SearchTruckViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return truckers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(foodTruckCellID, forIndexPath: indexPath) as! FoodTruckerTableViewCell
        
        let trucker = truckers[indexPath.row]
        cell.nameLabel.text = trucker.truckername
        cell.addressLabel.text = trucker.truckerDecs
        cell.favoriteButton.selected = trucker.liked
        cell.rateButton.setTitle("\(trucker.averageRate)", forState: UIControlState.Normal)
        if trucker.truckerImages.count > 0 {
            let truckerImageUrl = trucker.truckerImages[0].image?.large
            cell.truckImageView.kf_setImageWithURL(NSURL(string: truckerImageUrl!)!)
        }
        else {
            cell.truckImageView.image = UIImage(named: "truckerPlaceholder")
        }
        
        return cell
    }
}

extension SearchTruckViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        selectedTrucker = truckers[indexPath.row]
        
        performSegueWithIdentifier("PushTruckerDetailVC", sender: nil)
    }
}

extension SearchTruckViewController: FilterTableViewControllerDelegate {
    func filterControllerDidSelectedCity(city: City) {
        selectedCity = city
        cityButton.setTitle(city.cityName, forState: UIControlState.Normal)
        
        popoverContent.dismissViewControllerAnimated(true, completion: nil)
        
        searchTruckers()
    }
    
    func filterControllerDidSelectedCountry(country: Country) {
        selectedCountry = country
        countryButton.setTitle(country.countryName, forState: UIControlState.Normal)
        
        let cities = DataManager.sharedInstance.getLocalCitiesByCountry(selectedCountry.countryId)
        if cities.count > 0 {
            self.selectedCity = cities[0]
            self.cityButton.setTitle(self.selectedCity.cityName, forState: UIControlState.Normal)
            searchTruckers()
        }
        else {
            DataManager.sharedInstance.getCitiesByCountryId(self.selectedCountry.countryId,
                completionHandler: { (result, error) -> Void in
                    let cities = result as? [City]
                    if let cities = cities {
                        if cities.count > 0 {
                            self.selectedCity = cities[0]
                            self.cityButton.setTitle(self.selectedCity.cityName,
                                forState: UIControlState.Normal)
                            self.searchTruckers()
                        }
                    }
            })
        }
        popoverContent.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func filterControllerDidSelectedCategory(category: Category) {
        selectedCategory = category
        
        if category.categoryName == "None" {
            truckTypeButton.setTitle("Business type", forState: UIControlState.Normal)
        }
        else {
            truckTypeButton.setTitle(category.categoryName, forState: UIControlState.Normal)
        }
        
        popoverContent.dismissViewControllerAnimated(true, completion: nil)
        
        searchTruckers()
    }
    
    func filterControllerDidSelectedFoodType(foodType: FoodType) {
        selectedFoodType = foodType
        
        if foodType.foodTypeName == "None" {
            foodTypeButton.setTitle("Food Categories", forState: UIControlState.Normal)
        }
        else {
            foodTypeButton.setTitle(foodType.foodTypeName, forState: UIControlState.Normal)
        }
        
        popoverContent.dismissViewControllerAnimated(true, completion: nil)
        
        searchTruckers()
    }
}

// MARK: - UIPopoverControllerDelegate
extension SearchTruckViewController: UIPopoverPresentationControllerDelegate {
    func popoverPresentationControllerDidDismissPopover(popoverPresentationController: UIPopoverPresentationController) {
        //do som stuff from the popover
    }
}

extension SearchTruckViewController: FBAdViewDelegate {
    func adViewDidLoad(adView: FBAdView) {
        print("adViewDidLoad")
        adsView.hidden = false
    }
    
    func adView(adView: FBAdView, didFailWithError error: NSError) {
        print("didFailWithError")
        adsView.hidden = true
    }
}