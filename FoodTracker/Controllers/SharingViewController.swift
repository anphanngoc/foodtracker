//
//  SharingViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/20/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import Social
import MessageUI

class SharingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func shareEmailButtonDidTouched(sender: AnyObject) {
        guard MFMailComposeViewController.canSendMail() == true else {
            showOkeyMessage("Could Not Send Email",
                message: "Your device could not send e-mail. Please check e-mail configuration and try again.")
            return
        }
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setSubject("TastyTrackr")
        mailComposerVC.setMessageBody("Hey! I've been using TastyTrackr to track and follow my favorite food trucks, stalls and restaurants. Visit http://tastytrackr.co/", isHTML: false)
        self.presentViewController(mailComposerVC, animated: true, completion: nil)
    }
    
    @IBAction func shareFBButtonDidTouched(sender: AnyObject) {
        let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        let initialText = "Hey! I've been using TastyTrackr to track and follow my favorite food trucks, stalls and restaurants. Visit http://tastytrackr.co/"
        vc.setInitialText(initialText)
        //        vc.addImage(detailImageView.image!)
        vc.addURL(NSURL(string: "http://tastytrackr.co/"))
        presentViewController(vc, animated: true, completion: nil)
    }
}

// MARK: - MFMailComposeViewControllerDelegate
extension SharingViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
}
