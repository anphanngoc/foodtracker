//
//  SignInViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/13/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

protocol SignInViewControllerDelegate {
    func signInVCDidTouchedOnSignUpButton()
}

class SignInViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var signUpLabel: UILabel!
    
    // MARK: - Private properties
    private var email = ""
    private var password = ""
    
    var delegate:SignInViewControllerDelegate?
    var isWelcome = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.tableFooterView = UIView()
        
        let signUp = NSMutableAttributedString(string: signUpLabel.text!)
        let signUpRange: NSRange = (signUpLabel.text! as NSString).rangeOfString("Sign Up")
        signUp.addAttribute(NSForegroundColorAttributeName, value: UIColor.mainColor(), range:signUpRange)
        signUpLabel.attributedText = signUp
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func signInButtonDidTouched(sender: AnyObject) {
        signInUser()
    }

    @IBAction func signUpButtonDidTouched(sender: AnyObject) {
        if isWelcome {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let signUpVC = storyboard.instantiateViewControllerWithIdentifier("SignUpVCID") as! SignUpViewController
            navigationController?.pushViewController(signUpVC, animated: true)
        }
        else {
            navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func connectFBButtonDidTouched(sender: AnyObject) {
        loginToFacebookWithSuccess({ () -> () in
            //
            }) { (error: NSError?) -> () in
                //
        }
    }
    
    @IBAction func unwindToSignInVC(segue: UIStoryboardSegue) {
    }
    
    func textFieldDidChangedValue(textField: UITextField) {
        if textField.tag == 100 {
            email = textField.text ?? ""
        }
        else {
            password = textField.text ?? ""
        }
        
        if email.isEmpty || password.isEmpty {
            // TODO: Auto enable/disable button
        }
    }
    
    // MARK: - Helpers
    func loginToFacebookWithSuccess(successBlock: () -> (), andFailure failureBlock: (NSError?) -> ()) {
        
        if FBSDKAccessToken.currentAccessToken() != nil {
            //For debugging, when we want to ensure that facebook login always happens
            FBSDKLoginManager().logOut()
            //Otherwise do:
            //return
        }
        
        let fbReadPermissions = ["public_profile", "email", "user_friends"]
        let fbLogin = FBSDKLoginManager()
        fbLogin.logInWithReadPermissions(fbReadPermissions, fromViewController: nil) { (result: FBSDKLoginManagerLoginResult!, error: NSError!) -> Void in
            if error != nil {
                FBSDKLoginManager().logOut()
                failureBlock(error)
            }
            else if result.isCancelled {
                // Handle cancellations
                FBSDKLoginManager().logOut()
                failureBlock(nil)
            }
            else {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                var allPermsGranted = true
                
                //result.grantedPermissions returns an array of _NSCFString pointers
                let grantedPermissions = Array(result.grantedPermissions).map( {"\($0)"} )
                for permission in fbReadPermissions {
                    if !grantedPermissions.contains(permission) {
                        allPermsGranted = false
                        break
                    }
                }
                if allPermsGranted {
                    // Do work
                    _ = result.token.tokenString
                    _ = result.token.userID
                    
                    self.fetchUserInfo()
                    
                    successBlock()
                }
                else {
                    //The user did not grant all permissions requested
                    //Discover which permissions are granted
                    //and if you can live without the declined ones
                    
                    failureBlock(nil)
                }
            }
        }
    }
    
    func fetchUserInfo() {
        if((FBSDKAccessToken.currentAccessToken()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil){
                    print(result)
                    let fbUserEmail = result.valueForKey("email") as? String ?? ""
                    let fbUserId = result.valueForKey("id") as! String
                    let fbUserName = result.valueForKey("name") as! String
                    self.showLoading()
                    DataManager.sharedInstance.connectFacebookWithEmail(fbUserEmail, userName: fbUserName, fbUserId: fbUserId,
                        completeHandler: { (result, error) -> Void in
                        if error != nil {
                            // Handle error
                            self.hideLoading()
                            self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
                        }
                        else {
                            // Handle succeed
                            self.hideLoading()
                            AppDelegate.sharedAppDelegate().setHome()
                            print(result)
                        }
                    })
                }
            })
        }
    }
    
    func signInUser() {
        guard !email.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "Email is blank!")
            return
        }
        
        guard !password.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "Password is blank!")
            return
        }
        
        guard password.characters.count >= 6 else {
            showOkeyMessage(ERROR_TITLE, message: "Password less than 6 characters!")
            return
        }
        
        guard email.isValidEmail() else {
            showOkeyMessage(ERROR_TITLE, message: "Please enter a valid email!")
            return
        }
        
        showLoading()
        DataManager.sharedInstance.loginUserWithEmail(email, password: password) { (result, error) -> Void in
            if error != nil {
                self.hideLoading()
                self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
            }
            else {
                self.hideLoading()
                AppDelegate.sharedAppDelegate().setHome()
            }
        }
    }
}

extension SignInViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "InputCellID"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! InputTableViewCell
        
        cell.textField?.addTarget(self, action: "textFieldDidChangedValue:", forControlEvents: UIControlEvents.EditingChanged)
        if (indexPath.row == 0) {
            cell.textField?.tag = 100
            cell.textField?.placeholder = "Email"
            cell.textField?.autocapitalizationType = UITextAutocapitalizationType.None;
            cell.textField?.autocorrectionType = UITextAutocorrectionType.No;
            cell.textField?.keyboardType = UIKeyboardType.EmailAddress
        }
        else if (indexPath.row == 1) {
            cell.textField?.tag = 200
            cell.textField?.placeholder = "Password"
            cell.textField?.secureTextEntry = true
            cell.textField?.returnKeyType = UIReturnKeyType.Go
            cell.textField?.delegate = self
        }
        
        return cell
    }
}

extension SignInViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.returnKeyType == UIReturnKeyType.Go {
            textField.resignFirstResponder()
            signInButtonDidTouched(UIButton())
        }
        
        return true
    }
}
