//
//  SignUpViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/12/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

protocol SignUpViewControllerDelegate {
    func signUpVCDidTouchedOnSigninButton()
}

class SignUpViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var connectFBButton: UIButton!
    @IBOutlet weak var tableViewTopConstrait: NSLayoutConstraint!
    
    // MARK: - Private properties
    private var username = ""
    private var email = ""
    private var password = ""
    private var passwordConfirmation = ""
    
    var delegate: SignUpViewControllerDelegate?
    var isWelcome = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.tableFooterView = UIView()
        if (DeviceType.IS_IPHONE_4_OR_LESS) {
            tableViewTopConstrait.constant = 0.0
            connectFBButton.layoutIfNeeded()
        }
        
        let signIn = NSMutableAttributedString(string: signInLabel.text!)
        let signInRange: NSRange = (signInLabel.text! as NSString).rangeOfString("Sign In")
        signIn.addAttribute(NSForegroundColorAttributeName, value: UIColor.mainColor(), range:signInRange)
        signInLabel.attributedText = signIn
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    
    @IBAction func signUpButtonDidTouched(sender: AnyObject) {
        signUpUser()
    }

    @IBAction func signInButtonDidTouched(sender: AnyObject) {
        if isWelcome {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let signInVC = storyboard.instantiateViewControllerWithIdentifier("SignInVCID") as! SignInViewController
            navigationController?.pushViewController(signInVC, animated: true)
        }
        else {
            navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func connectFBButtonDidTouched(sender: AnyObject) {
        loginToFacebookWithSuccess({ () -> () in
            //
            }) { (error: NSError?) -> () in
                //
        }
    }
    
    func textFieldDidChangedValue(textField: UITextField) {
        if textField.tag == 100 {
            username = textField.text ?? ""
        }
        else if textField.tag == 200 {
            email = textField.text ?? ""
        }
        else if textField.tag == 300 {
            password = textField.text ?? ""
        }
        else {
            passwordConfirmation = textField.text ?? ""
        }
        
        if username.isEmpty || email.isEmpty || password.isEmpty || passwordConfirmation.isEmpty {
            // TODO: Auto enable/disable button
        }
    }
    
    func signUpUser() {
        guard !username.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "Name is blank!")
            return
        }
        
        guard !email.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "Email is blank!")
            return
        }
        
        guard !password.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "Password is blank!")
            return
        }
        
        guard password.characters.count >= 6 else {
            showOkeyMessage(ERROR_TITLE, message: "Password less than 6 characters!")
            return
        }
        
        guard !passwordConfirmation.isEmpty else {
            showOkeyMessage(ERROR_TITLE, message: "Password confirmation is blank!")
            return
        }
        
        guard passwordConfirmation.characters.count >= 6 else {
            showOkeyMessage(ERROR_TITLE, message: "Password confirmation less than 6 characters!")
            return
        }
        
        guard email.isValidEmail() else {
            showOkeyMessage(ERROR_TITLE, message: "Please enter a valid email!")
            return
        }
        
        showLoading()
        DataManager.sharedInstance.signUpUserWithEmail(email, userName: username, phone: "", password: password, passwordConfirmation: passwordConfirmation) { (result, error) -> Void in
            if error != nil {
                self.hideLoading()
                self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
            }
            else {
                self.hideLoading()
                AppDelegate.sharedAppDelegate().setHome()
            }
        }
    }
    
    // MARK: - Helpers
    func loginToFacebookWithSuccess(successBlock: () -> (), andFailure failureBlock: (NSError?) -> ()) {
        
        if FBSDKAccessToken.currentAccessToken() != nil {
            //For debugging, when we want to ensure that facebook login always happens
            FBSDKLoginManager().logOut()
            //Otherwise do:
            //return
        }
        
        let fbReadPermissions = ["public_profile", "email", "user_friends"]
        let fbLogin = FBSDKLoginManager()
        fbLogin.logInWithReadPermissions(fbReadPermissions, fromViewController: nil) { (result: FBSDKLoginManagerLoginResult!, error: NSError!) -> Void in
            if error != nil {
                FBSDKLoginManager().logOut()
                failureBlock(error)
            }
            else if result.isCancelled {
                // Handle cancellations
                FBSDKLoginManager().logOut()
                failureBlock(nil)
            }
            else {
                // If you ask for multiple permissions at once, you
                // should check if specific permissions missing
                var allPermsGranted = true
                
                //result.grantedPermissions returns an array of _NSCFString pointers
                let grantedPermissions = Array(result.grantedPermissions).map( {"\($0)"} )
                for permission in fbReadPermissions {
                    if !grantedPermissions.contains(permission) {
                        allPermsGranted = false
                        break
                    }
                }
                if allPermsGranted {
                    // Do work
                    _ = result.token.tokenString
                    _ = result.token.userID
                    
                    self.fetchUserInfo()
                    
                    successBlock()
                }
                else {
                    //The user did not grant all permissions requested
                    //Discover which permissions are granted
                    //and if you can live without the declined ones
                    
                    failureBlock(nil)
                }
            }
        }
    }
    
    func fetchUserInfo(){
        if((FBSDKAccessToken.currentAccessToken()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).startWithCompletionHandler({ (connection, result, error) -> Void in
                if (error == nil){
                    print(result)
                    let fbUserEmail = result.valueForKey("email") as? String ?? ""
                    let fbUserId = result.valueForKey("id") as! String
                    let fbUserName = result.valueForKey("name") as! String
                    self.showLoading()
                    DataManager.sharedInstance.connectFacebookWithEmail(fbUserEmail, userName: fbUserName, fbUserId: fbUserId,
                        completeHandler: { (result, error) -> Void in
                            if error != nil {
                                // Handle error
                                self.hideLoading()
                                self.showOkeyMessage(ERROR_TITLE, message: Utilities.getErrorMessage(error))
                            }
                            else {
                                // Handle succeed
                                self.hideLoading()
                                AppDelegate.sharedAppDelegate().setHome()
                                print(result)
                            }
                    })

                }
            })
        }
    }
}

extension SignUpViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "InputCellID"
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! InputTableViewCell
        
        cell.textField?.addTarget(self, action: "textFieldDidChangedValue:", forControlEvents: UIControlEvents.EditingChanged)
        if (indexPath.row == 0) {
            cell.textField?.placeholder = "Name"
            cell.textField?.tag = 100
        }
        else if (indexPath.row == 1) {
            cell.textField?.placeholder = "Email"
            cell.textField?.autocapitalizationType = UITextAutocapitalizationType.None;
            cell.textField?.autocorrectionType = UITextAutocorrectionType.No;
            cell.textField?.keyboardType = UIKeyboardType.EmailAddress
            cell.textField?.tag = 200
        }
        else if (indexPath.row == 2) {
            cell.textField?.placeholder = "Password"
            cell.textField?.secureTextEntry = true
            cell.textField?.tag = 300
        }
        else if (indexPath.row == 3) {
            cell.textField?.placeholder = "Confirm Password"
            cell.textField?.returnKeyType = UIReturnKeyType.Go
            cell.textField?.delegate = self
            cell.textField?.secureTextEntry = true
            cell.textField?.tag = 400
        }
        
        return cell
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField.returnKeyType == UIReturnKeyType.Go {
            signUpButtonDidTouched(UIButton())
        }
        
        return true
    }
}
