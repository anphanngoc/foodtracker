//
//  TruckerDetailViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/15/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import ObjectMapper
import Social
import MessageUI

class TruckerDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var isLiked = false
    
    var trucker: Trucker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isLiked = trucker.liked
        title = trucker.truckername
        
        // Do any additional setup after loading the view.
        tableView.registerNib(UINib(nibName: "ReviewTableViewCell", bundle: nil),
            forCellReuseIdentifier: "ReviewCellID")
        tableView.registerNib(UINib(nibName: "MenuTableViewCell", bundle: nil),
            forCellReuseIdentifier: "MenuCellID")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        DataManager.sharedInstance.getTruckerDetailById(trucker.truckerId) { (result, error) -> Void in
            if error == nil {
                self.trucker = result as! Trucker
                
                var carouselImages = [UIImage]()
                for i in 0..<self.trucker.truckerImages.count {
                    let urlImage = self.trucker.truckerImages[i].image!.large
                    HTTPClient.sharedInstance.downloadImagefromURL(urlImage, completionHandler: { (result, error) -> Void in
                        if let image = result as? UIImage {
                            carouselImages.append(image)
                        }
                    })
                }
                
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    @IBAction func unwindToDetailVC(segue: UIStoryboardSegue) {
    }
    
    @IBAction func backButtonDidTouched(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "PushReviewVC" {
            if let reviewVC = segue.destinationViewController as? ReviewViewController {
                reviewVC.trucker = trucker
            }
        }
        else if segue.identifier == "PushMapVC" {
            if let mapVC = segue.destinationViewController as? MapViewController {
                mapVC.trucker = trucker
            }
        }
    }
    
    // MARK: - Private methods
    private func callNumber(phoneNumber: String) {
        if let phoneCallURL: NSURL = NSURL(string: "telprompt://\(phoneNumber)") {
            let application: UIApplication = UIApplication.sharedApplication()
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL);
            }
            else {
                showOkeyMessage(ERROR_TITLE, message: "Your device doesn't support call!")
            }
        }
        else {
            showOkeyMessage(ERROR_TITLE, message: "Phone number is not in the correct format!")
        }
    }
}

extension TruckerDetailViewController: UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 || section == 1 || section == 2 || section == 4  {
            return 1
        }
        else if section == 3 {  // Trucker info section
            return 4
        }
        else if section == 5 {  // Menu list
            return trucker.items.count
        }
        else {  // Comments
            return trucker.comments.count
        }
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.selectionStyle = UITableViewCellSelectionStyle.None
        
        if indexPath.section == 1 || indexPath.section == 2 { // What's new & desc
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.font = UIFont.openSansRegular(size: 14)
            cell.textLabel?.textColor = UIColor.rgbColor(red: 142, green: 142, blue: 142)
        }
        else if indexPath.section == 3 { // Info section
            cell.selectionStyle = UITableViewCellSelectionStyle.Gray
            cell.textLabel?.font = UIFont.openSansRegular(size: 14)
            cell.detailTextLabel?.font = UIFont.openSansRegular(size: 14)
            cell.detailTextLabel?.textColor = UIColor.mainColor()
        }
        else if indexPath.section == 5 {
            cell.imageView?.frame = CGRectMake(15.0, 7.0, 35.0, 35.0);
            cell.imageView?.layer.cornerRadius = 17.5
            cell.imageView?.clipsToBounds = true
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("TruckerInfoCellID",
                forIndexPath: indexPath) as! TruckerInfoTableViewCell
            cell.delegate = self
            cell.setInfoTrucker(trucker)

            return cell
        }
        else if indexPath.section == 1 {    // What's new
            var cell = tableView.dequeueReusableCellWithIdentifier("WhatNewCellID")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "WhatNewCellID")
                cell?.textLabel?.numberOfLines = 0
            }
            
            cell?.textLabel?.text = trucker.what_new
            
            return cell!
        }
        else if indexPath.section == 2 {    // Description
            var cell = tableView.dequeueReusableCellWithIdentifier("DescCellID")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "DescCellID")
                cell?.textLabel?.numberOfLines = 0
            }
            
            cell?.textLabel?.text = trucker.truckerDecs
            
            return cell!
        }
        else if indexPath.section == 3 {    // Trucker's info
            var cell = tableView.dequeueReusableCellWithIdentifier("InfoCellID")
            if cell == nil {
                cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "InfoCellID")
            }
            cell?.selectionStyle = UITableViewCellSelectionStyle.Default
            cell?.textLabel?.text = ""
            cell?.detailTextLabel?.text = ""
            
            if indexPath.row == 0 {
                cell?.textLabel?.text = "Address"
                cell?.detailTextLabel?.text = trucker.address.street
            }
            else if indexPath.row == 1 {
                if !trucker.phone.isEmpty {
                    cell?.textLabel?.text = "Phone"
                    cell?.detailTextLabel?.text = trucker.phone
                }
            }
            else if indexPath.row == 2 {
                if !trucker.website.isEmpty {
                    cell?.textLabel?.text = "Website"
                    cell?.detailTextLabel?.text = trucker.website
                }
            }
            else {
                if !trucker.email.isEmpty {
                    cell?.textLabel?.text = "Email"
                    cell?.detailTextLabel?.text = trucker.email
                }
            }
            
            return cell!
        }
        else if indexPath.section == 4 {
            let cell = tableView.dequeueReusableCellWithIdentifier("ShareSocialCellID",
                forIndexPath: indexPath) as! ShareSocialTableViewCell
            cell.delegate = self
            
            return cell
        }
        else if indexPath.section == 5 {    // Menu list
            let cell = tableView.dequeueReusableCellWithIdentifier("MenuCellID",
                forIndexPath: indexPath) as! MenuTableViewCell
            let item = trucker.items[indexPath.row]
            cell.foodNameLabel.text = item.itemName
            cell.priceLabel.text = "\(item.itemPrice)" + " " + item.currency
            cell.foodImageView.kf_setImageWithURL(NSURL(string: (item.itemImage?.large)!)!)
            
            return cell
        }
        else {  // Comments
            let cell = tableView.dequeueReusableCellWithIdentifier("ReviewCellID",
                forIndexPath: indexPath) as! ReviewTableViewCell
            let comment = trucker.comments[indexPath.row]
            cell.nameLabel.text = comment.user.userName
            cell.commentLabel.text = comment.content
            cell.rateButton.setTitle(String(comment.rate), forState: UIControlState.Normal)
            cell.avatarImageView.kf_setImageWithURL(NSURL(string: comment.user.avatar.large)!, placeholderImage: UIImage(named: "avatarDefault"))
            
            return cell
        }
    }
}

// MARK: -  UITableViewDelegate
extension TruckerDetailViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        // Handle actions to contact information
        if indexPath.section == 3 {
            if indexPath.row == 1 {
                // Call
                if trucker.phone.characters.count > 0 {
                    var phoneNumber = trucker.phone
                    if phoneNumber.hasPrefix("(") {
                        phoneNumber = String(phoneNumber.characters.dropFirst()).replace(")", withString: "")
                    }
                    callNumber(phoneNumber)
                }
            }
            else if indexPath.row == 2 {
                // Open website
                if trucker.website.characters.count > 0 {
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let websiteVC = storyboard.instantiateViewControllerWithIdentifier("WebViewControllerID") as! WebViewController
                    navigationController?.pushViewController(websiteVC, animated: true)
                    websiteVC.baseUrlString = trucker.website
                }
            }
            else if indexPath.row == 3 {
                // Email
                guard MFMailComposeViewController.canSendMail() == true else {
                    showOkeyMessage("Could Not Send Email",
                        message: "Your device could not send e-mail. Please check e-mail configuration and try again.")
                    return
                }
                
                if trucker.email.characters.count > 0 {
                    let mailComposerVC = MFMailComposeViewController()
                    mailComposerVC.mailComposeDelegate = self
                    mailComposerVC.setSubject("TastyTrackr")
                    mailComposerVC.setToRecipients([trucker.email])
                    self.presentViewController(mailComposerVC, animated: true, completion: nil)
                }
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if (indexPath.section == 0) {
            return 320.0
        }
        else if indexPath.section == 1 {
            if trucker.what_new.characters.count == 0 {
                return 0
            }
            
            let height = trucker.truckerDecs.heightWithWidth(CGRectGetWidth(self.tableView.frame),
                font: UIFont.openSansRegular(size: 15))
            
            return height + 5
        }
        else if indexPath.section == 2 {
            if trucker.truckerDecs.isEmpty {
                return 0
            }

            let height = trucker.truckerDecs.heightWithWidth(CGRectGetWidth(self.tableView.frame),
                font: UIFont.openSansRegular(size: 15))
            return height + 5
        }
        else if indexPath.section == 3 { // Contact information
            if (indexPath.row == 1 && trucker.phone.isEmpty) ||
            (indexPath.row == 2 && trucker.website.isEmpty) ||
            (indexPath.row == 3 && trucker.email.isEmpty) {
                return 0
            }
            else {
                return 44.0
            }
        }
        else if indexPath.section == 4 {    // Sharing
            return 80.0
        }
        else if indexPath.section == 5 {
            if trucker.items.count == 0 { return 0 }
            
            return 50.0
        }
        else if indexPath.section == 6 {
            if trucker.comments.count == 0 { return 0 }
            
            return 50.0
        }
        
        return 44.0
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            if trucker.what_new.isEmpty {
                return nil
            }
            
            return "What's new?"
        }
        else if section == 2 {
            if trucker.truckerDecs.isEmpty {
                return nil
            }
            
            return "About"
        }
        else if section == 3 {
            return "Contact Information"
        }
        else if section == 4 {
            return "Share"
        }
        else if section == 5 {
            if trucker.items.count  == 0 { return nil }
            
            return "Menu"
        }
        else if section == 6 {
            if trucker.comments.count  == 0 { return nil }
            
            return "Reviews"
        }
        else {
            return nil
        }
    }
}

// MARK: - TruckerInfoTableViewCellDelegate
extension TruckerDetailViewController: TruckerInfoTableViewCellDelegate {
    func truckerCellDidTouchedOnContactButton(button: UIButton) {
        if trucker.phone.characters.count > 0 {
            var phoneNumber = trucker.phone
            if phoneNumber.hasPrefix("(") {
                phoneNumber = String(phoneNumber.characters.dropFirst()).replace(")", withString: "")
            }
            callNumber(phoneNumber)
        }
    }
    
    func truckerCellDidTouchedOnDirectionButton(button: UIButton) {
        print(button)
    }
    
    func truckerCellDidTouchedOnFavoriteButton(button: DOFavoriteButton) {
        print(button)
        let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as! TruckerInfoTableViewCell
        
        if isLiked {
            isLiked = false
            DataManager.sharedInstance.removeFavoriteTrucker(trucker.truckerId) { (result, error) -> Void in
                if Utilities.getErrorMessage(error) == "Unlike successful" {
                    cell.setFavoriteTrucker(false)
                }
                else {
                    self.showOkeyMessage(FOOD_TRUCKER, message: Utilities.getErrorMessage(error))
                }
            }
        }
        else {
            isLiked = true
            DataManager.sharedInstance.favoriteTrucker(trucker.truckerId) { (result, error) -> Void in
                cell.setFavoriteTrucker(true)
                if Utilities.getErrorMessage(error) == "Like successful" {
                    cell.setFavoriteTrucker(true)
                }
                else {
                    self.showOkeyMessage(FOOD_TRUCKER, message: Utilities.getErrorMessage(error))
                }
            }
        }
    }
}

// MARK: - ShareSocialTableViewCellDelegate
extension TruckerDetailViewController: ShareSocialTableViewCellDelegate {
    func shareSocialDidTouchedOnFBButton(button: AnyObject) {
        let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        let initialText = "Hey! Follow updates of \(trucker.truckername) by downloading this awesome app at app.tastytrackr.co/share/\(trucker.truckerId)"
        vc.setInitialText(initialText)
        vc.addURL(NSURL(string: "http://tastytrackr.co/"))
        presentViewController(vc, animated: true, completion: nil)
    }
    
    func shareSocialDidTouchedOnTwitterButton(button: AnyObject) {
        let vc = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        let initialText = "Hey! Follow updates of \(trucker.truckername) by downloading this awesome app at app.tastytrackr.co/share/\(trucker.truckerId)"
        vc.setInitialText(initialText)
        presentViewController(vc, animated: true, completion: nil)
    }
    
    func shareSocialDidTouchedOnWhatsAppButton(button: AnyObject) {
        let initialText: NSString = "Hey! Follow updates of \(trucker.truckername) by downloading this awesome app at app.tastytrackr.co/share/\(trucker.truckerId)"
        let titlewithoutspace = initialText.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
        if let titlewithoutspace = titlewithoutspace {
            let urlWhats = NSString(string: "whatsapp://send?text=\(titlewithoutspace)")
            let whatsappURL = NSURL(string: urlWhats as String)
            
            if UIApplication.sharedApplication().canOpenURL(whatsappURL!) {
                UIApplication.sharedApplication().openURL(whatsappURL!)
            }
            else {
                showOkeyMessage("WhatsApp not installed.",
                    message: "Your device has no WhatsApp installed.")
            }
        } else {
            // Unwrapping failed because titlewithoutspace is nil (might be because stringByAddingPercentEscapesUsingEncoding failed).
        }
    }
    
    func shareSocialDidTouchedOnEmailButton(button: AnyObject) {
        guard MFMailComposeViewController.canSendMail() == true else {
            showOkeyMessage("Could Not Send Email",
                message: "Your device could not send e-mail. Please check e-mail configuration and try again.")
            return
        }
        
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self
        mailComposerVC.setSubject("TastyTrackr")
        mailComposerVC.setMessageBody("Hey! Follow updates of \(trucker.truckername) by downloading this awesome app at app.tastytrackr.co/share/\(trucker.truckerId)", isHTML: false)
        self.presentViewController(mailComposerVC, animated: true, completion: nil)
    }
}

// MARK: - MFMailComposeViewControllerDelegate
extension TruckerDetailViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}