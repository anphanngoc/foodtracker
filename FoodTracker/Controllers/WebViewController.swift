//
//  WebViewController.swift
//  FoodTracker
//
//  Created by An Phan on 12/8/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    var activityIndicator: UIActivityIndicatorView!
    var indicatorBarItem: UIBarButtonItem!
    var refreshBarItem: UIBarButtonItem!
    
    var baseUrlString: String = "http://tastytrackr.co/mob/"
    var detailUrlString: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch detailUrlString {
        case "about.html":
            title = "About"
        case "faq.html":
            title = "FAQ"
        case "tnc.html":
            title = "Terms & Conditions"
        case "privacy.html":
            title = "Privacy Policy"
        default:
            title = "Website"
            
        }

        // Add right bar button on navigation bar.
        refreshBarItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Refresh, target: self, action: "refreshButtonItemDidTouch:")
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        indicatorBarItem = UIBarButtonItem(customView: activityIndicator)
        indicatorBarItem.style = UIBarButtonItemStyle.Plain
        navigationItem.rightBarButtonItem = self.indicatorBarItem;
        
        let urlString = baseUrlString + detailUrlString
        let urlRequest = NSURLRequest(URL: NSURL(string: urlString)!)
        webView.loadRequest(urlRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func refreshButtonItemDidTouch(sender: AnyObject) {
        webView.reload()
        activityIndicator.startAnimating()
        navigationItem.rightBarButtonItem = nil
    }
    
    @IBAction func backButtonDidTouched(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
}

extension WebViewController: UIWebViewDelegate {
    func webView(webView: UIWebView, shouldStartLoadWithRequest
        request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        activityIndicator.startAnimating()
        navigationItem.rightBarButtonItem = nil
        navigationItem.rightBarButtonItem = indicatorBarItem
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        activityIndicator.stopAnimating()
        navigationItem.rightBarButtonItem = refreshBarItem
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError?) {
        activityIndicator.stopAnimating()
        navigationItem.rightBarButtonItem = refreshBarItem
    }
}
