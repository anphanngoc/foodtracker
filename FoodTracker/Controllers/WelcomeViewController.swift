//
//  WelcomeViewController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/11/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    /// A button to go to the sign-up flow
    @IBOutlet weak var signUpButton: UIButton!
    /// A button to go to the sign-in flow
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.signUpButton.backgroundColor = .rgbaColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.2);
        self.signInButton.backgroundColor = .rgbaColor(red: 255.0, green: 255.0, blue: 255.0, alpha: 0.2);
        
        var imageLeftInset: CGFloat = 250.0
        var titleRightInset: CGFloat = 190.0
        if (DeviceType.IS_IPHONE_6P) {
            imageLeftInset = 330.0
            titleRightInset = 270.0
        }
        else if (DeviceType.IS_IPHONE_6) {
            imageLeftInset = 300.0
            titleRightInset = 230.0
        }
        
        if DeviceType.IS_IPHONE_4_OR_LESS {
            logoTopConstraint.constant = 10
        }
        
        signInButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, titleRightInset)
        signInButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, imageLeftInset, 0.0, 0.0)
        signUpButton.titleEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, titleRightInset)
        signUpButton.imageEdgeInsets = UIEdgeInsetsMake(0.0, imageLeftInset, 0.0, 0.0)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    @IBAction func unwindToWelcomeVC(segue: UIStoryboardSegue) {
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "PushSignInVCSegue" {
            if let signInVC = segue.destinationViewController as? SignInViewController {
                signInVC.delegate = self
                signInVC.isWelcome = true
            }
        }
        else if segue.identifier == "PushSignUpVCSegue" {
            if let signUpVC = segue.destinationViewController as? SignUpViewController {
                signUpVC.delegate = self
                signUpVC.isWelcome = true
            }
        }
    }
}

extension WelcomeViewController: SignInViewControllerDelegate {
    func signInVCDidTouchedOnSignUpButton() {
        performSegueWithIdentifier("PushSignUpVCSegue", sender: nil)
    }
}

extension WelcomeViewController: SignUpViewControllerDelegate {
    func signUpVCDidTouchedOnSigninButton() {
        performSegueWithIdentifier("PushSignInVCSegue", sender: nil)
    }
}
