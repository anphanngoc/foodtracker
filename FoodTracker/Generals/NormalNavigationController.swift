//
//  NormalNavigationController.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/12/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

class NormalNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationBar.barTintColor = .mainColor()
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(),
            NSFontAttributeName: UIFont.openSansRegular(size: 14)]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
