//
//  Address.swift
//  FoodTracker
//
//  Created by An Phan on 11/21/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import ObjectMapper

class Address: Mappable {
    var addressId = 0
    var longitude = ""
    var latitude = ""
    var street = ""
    var state = ""
    var city = ""
    var area = ""
    var country = ""
    
    required init() {
        
    }
    
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        addressId   <- map["id"]
        longitude   <- map["longitude"]
        latitude    <- map["latitude"]
        street      <- map["street"]
        state       <- map["state"]
        city        <- map["city"]
        area        <- map["area"]
        country      <- map["country"]
    }
    
    func getAddress() -> String {
        var address = ""
        if street != "" {
            let street:String! = self.street
            address = "\(address)\(street), "
        }
        if state != "" {
            let state = self.state
            address = "\(address)\(state), "
        }
        if city != "" {
            let city = self.city
            address = "\(address)\(city), "
        }
        if area != "" {
            let area = self.area
            address = "\(address)\(area), "
        }
        if country != "" {
            let country = self.country
            address = "\(address)\(country)"
        }
        return address
    }
}
