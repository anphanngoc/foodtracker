//
//  Category.swift
//  FoodTracker
//
//  Created by An Phan on 11/29/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class Category: Mappable {
    var categoryId: Int = 0
    var categoryName = ""
    var categoryType = ""
    
    required init() {
        
    }
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        categoryId <- map["id"]
        categoryName <- map["truck_name"]
        categoryType <- map["truck_type"]
    }
}
