//
//  City.swift
//  FoodTracker
//
//  Created by An Phan on 11/22/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class City: Mappable {
    var cityId: Int = 0
    var cityName = ""
    var cityDecs = ""
    
    required init() {
        
    }
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        cityId <- map["id"]
        cityName <- map["name"]
        cityDecs <- map["description"]
    }
}
