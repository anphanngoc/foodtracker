//
//  Comment.swift
//  FoodTracker
//
//  Created by An Phan on 11/21/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import ObjectMapper

class Comment: Mappable {
    var commentId: Int?
    var truckerId: Int?
    var content: String?
    var rate = 0.0
    var commentDate: String?
    var user = User()
    
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        commentId   <- map["id"]
        truckerId   <- map["truck_id"]
        content     <- map["content"]
        rate        <- map["rate"]
        commentDate <- map["comment_date"]
        user        <- map["user"]
    }
}
