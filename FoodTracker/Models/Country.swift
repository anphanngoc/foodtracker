//
//  Country.swift
//  FoodTracker
//
//  Created by An Phan on 11/22/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class Country: Mappable {
    var countryId: Int = 0
    var countryName = ""
    var countryCode = ""
    var cities = [City]()
    
    required init() {
        
    }
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        countryId <- map["id"]
        countryName <- map["name"]
        countryCode <- map["code"]
    }
}
