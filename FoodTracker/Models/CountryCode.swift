//
//  CountryCode.swift
//  FoodTracker
//
//  Created by An Phan on 12/2/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class CountryCode: Mappable {
    var code = ""
    var dialCode = ""
    var name = ""
    
    required init() {
        
    }
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        code        <- map["code"]
        dialCode    <- map["dial_code"]
        name        <- map["name"]
    }
}