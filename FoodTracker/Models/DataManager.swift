//
//  DataManager.swift
//  FoodTracker
//
//  Created by An Phan on 11/19/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation
import ObjectMapper

typealias CompleteHandler = (result: AnyObject!, error: NSError!) -> Void

class DataManager {
    static let sharedInstance = DataManager()
    
    /// A flag indicate whether user authorized or not.
    var isAuthorized = false
    
    /// User logged in the app
    var loggedUser: User!
    
    /// List of truckers
    var truckers = [Trucker]()
    
    /// List of open truckers
    var openTruckers = NSMutableArray()
    
    /// List of close truckers
    var closeTruckers = NSMutableArray()
    
    /// List of favorited truckers
    var favoritedTruckers = NSMutableArray()
    
    /// List of countries
    var countries = [Country]()
    
    /// List of food type
    var foodTypeList = [FoodType]()
    
    /// Current location
    var currentLocation = CLLocation()
    
    /// List of truck type
    var categories = [Category]()
    
    /// Login user
    func loginUserWithEmail(email: String, password: String, completeHandler: CompleteHandler) {
        let params = [
            "email": email,
            "password": password,
            "device_type": "ios",
            "device_token": AppDelegate.sharedAppDelegate().deviceToken
        ]
        HTTPClient.sharedInstance.POST("users/sign_in", params: params) { (result, error) -> Void in
            if let _ = error {
                completeHandler(result: nil, error: error)
            }
            else {
                print(result)
                self.isAuthorized = true
                self.loggedUser = Mapper<User>().map(result["user"])
                HTTPClient.sharedInstance.setHeaderToken(self.loggedUser.token)
                
                // Save user
                let userDefault = NSUserDefaults.standardUserDefaults()
                userDefault.setInteger(self.loggedUser.userId, forKey: FoodTrackerUserIdKey)
                userDefault.setValue(self.loggedUser.token, forKey: FoodTrackerUserTokenKey)
                
                // Post notification to notify that user logged in
                NSNotificationCenter.defaultCenter().postNotificationName(FoodTrackerUpdateUserNotification, object: self, userInfo: nil)

                completeHandler(result: result, error: nil)
                print("User: \(result)")
            }
        }
    }
    
    // Sign up
    func signUpUserWithEmail(email: String, userName: String, phone: String, password: String, passwordConfirmation: String,
        completeHandler: CompleteHandler) {
        let params = [
            "email": email,
            "user_name": userName,
            "password": password,
            "password_confirmation": passwordConfirmation,
            "device_type": "ios",
            "device_token": AppDelegate.sharedAppDelegate().deviceToken
        ]
        HTTPClient.sharedInstance.POST("users", params: params) { (result, error) -> Void in
            if let _ = error {
                completeHandler(result: nil, error: error)
            }
            else {
                self.isAuthorized = true
                self.loggedUser = Mapper<User>().map(result["user"])
                HTTPClient.sharedInstance.setHeaderToken(self.loggedUser.token)
                
                // Save user
                let userDefault = NSUserDefaults.standardUserDefaults()
                userDefault.setInteger(self.loggedUser.userId, forKey: FoodTrackerUserIdKey)
                userDefault.setValue(self.loggedUser.token, forKey: FoodTrackerUserTokenKey)
                
                // Post notification to notify that user logged in
                NSNotificationCenter.defaultCenter().postNotificationName(FoodTrackerUpdateUserNotification, object: self, userInfo: nil)
                
                completeHandler(result: result, error: nil)
                print(result)
            }
        }
    }
    
    // Connect to Facebook
    func connectFacebookWithEmail(email: String, userName: String, fbUserId: String, completeHandler: CompleteHandler) {
        let params = [
            "email": email,
            "user_name": userName,
            "device_type": "ios",
            "device_token": AppDelegate.sharedAppDelegate().deviceToken,
            "uid": fbUserId
        ]
        HTTPClient.sharedInstance.POST("facebooks/connect_facebook", params: params) { (result, error) -> Void in
            if let _ = error {
                completeHandler(result: nil, error: error)
            }
            else {
                self.isAuthorized = true
                self.loggedUser = Mapper<User>().map(result["user"])
                HTTPClient.sharedInstance.setHeaderToken(self.loggedUser.token)
                
                // Save user
                let userDefault = NSUserDefaults.standardUserDefaults()
                userDefault.setInteger(self.loggedUser.userId, forKey: FoodTrackerUserIdKey)
                userDefault.setValue(self.loggedUser.token, forKey: FoodTrackerUserTokenKey)
                
                // Post notification to notify that user logged in
                NSNotificationCenter.defaultCenter().postNotificationName(FoodTrackerUpdateUserNotification, object: self, userInfo: nil)
                
                completeHandler(result: result, error: nil)
                print(result)
            }
        }
    }
    
    // Reset password
    func sendRequestResetPasswordToEmail(email: String, completeHandler: CompleteHandler) {
        let params = [
            "email": email,
        ]
        HTTPClient.sharedInstance.POST("users/reset_password", params: params) { (result, error) -> Void in
            if let _ = error {
                completeHandler(result: nil, error: error)
            }
            else {
                completeHandler(result: result, error: nil)
                print(result)
            }
        }
    }
    
    /*
    // Get list of truckers
    func getTruckersByLatitude(latitude: String, longtitude: String, completeHandler: CompleteHandler) {
        let query = "searchs/trucks_index?from_lat=\(latitude)&from_long=\(longtitude)"
        HTTPClient.sharedInstance.GET(query, params: nil) { (result, error) -> Void in
            if let _ = error {
                print(error)
                completeHandler(result: nil, error: error)
            }
            else {
                self.truckers = Mapper<Trucker>().mapArray(result["objects"])!
                print(self.truckers)
                
                completeHandler(result: result, error: nil)
                print(result)
            }
        }
    }
    */
    
    // Search trucker by lat, lng or city
    func getTruckersByLatitude(latitude: String, longtitude: String, city: String, truckType: String, openType: String, foodType: String, page: Int, perPage: Int, completeHandler: CompleteHandler) {
        var params = [String: AnyObject]()
        if city.characters.count > 0 {
            params["city"] = "\"" + "\(city)" + "\""
        }
        
        if truckType.characters.count > 0 {
            params["truck_type"] = truckType
        }
        
        if foodType.characters.count > 0 {
            params["food_type_id"] = foodType
        }
        
        if latitude.characters.count > 0 && longtitude.characters.count > 0 {
            params["from_lat"] = latitude
            params["from_long"] = longtitude
        }
        
        if openType.characters.count > 0 {
            params["open_type"] = openType
        }
        
        // TODO: Change this on production
        let timeZone = NSTimeZone.localTimeZone().name
        params["time_zone"] = timeZone  //"Asia/Singapore"
        if page > 0 {
            params["page"] = String(page)
        }
        
        if perPage > 0 {
            params["per_page"] = String(perPage)
        }
        
        HTTPClient.sharedInstance.GET("searchs/trucks_index", params: params) { (result, error) -> Void in
            if let _ = error {
                print(error)
                completeHandler(result: nil, error: error)
            }
            else {
                let truckers = Mapper<Trucker>().mapArray(result["objects"])!
                
                if page > 0 && perPage > 0 {    // For home page, otherwise for searching
                    if openType == "open_now" {
                        if page == 1 {  // Remove all current trucks if loading the first page
                            self.openTruckers.removeAllObjects()
                        }
                        self.openTruckers.addObjectsFromArray(truckers)
                    }
                    else {
                        if page == 1 {  // Remove all current trucks if loading the first page
                            self.closeTruckers.removeAllObjects()
                        }
                        self.closeTruckers.addObjectsFromArray(truckers)
                    }
                }
                
                completeHandler(result: truckers, error: nil)
                print(result)
            }
        }
    }
    
    // Get list of favorited truckers
    func getFavoritedTruckersWithPage(page: Int, perPage: Int, completionHandler: CompleteHandler) {
        let params = [
            "page": String(page),
            "per_page": String(perPage)
        ]
        
        HTTPClient.sharedInstance.GET("user_trucks", params: params) { (result, error) -> Void in
            if let _ = error {
                completionHandler(result: nil, error: error)
            }
            else {
                let favoritedTruckers = Mapper<Trucker>().mapArray(result["objects"])!
                
                if page == 1 {
                    self.favoritedTruckers.removeAllObjects()
                }
                self.favoritedTruckers.addObjectsFromArray(favoritedTruckers)
                
                completionHandler(result: favoritedTruckers, error: nil)
                print(result)
            }
        }
    }
    
    // Get a detail trucker
    func getTruckerDetailById(truckerId: Int, completionHandler: CompleteHandler) {
        
        HTTPClient.sharedInstance.GET("user_trucks/\(truckerId)", params: nil) { (result, error) -> Void in
            if let _ = error {
                completionHandler(result: nil, error: error)
            }
            else {
                let truckerDetail = Mapper<Trucker>().map(result["user_truck"])
                
                /*
                // Update truck in list of open truck
                if let truckerDetail = truckerDetail {
                    for (index, trucker) in self.openTruckers.enumerate() {
                        if trucker.truckerId == truckerDetail.truckerId {
                            self.truckers[index] = truckerDetail
                        }
                    }
                }
                */

                completionHandler(result: truckerDetail, error: nil)
                print(result)
            }
        }
    }
    
    // Update user info
    func updateUserInfoWithName(name: String, email: String, phone: String, gender: String, completionHandler: CompleteHandler) {
        let query = "users/\(loggedUser?.userId)"
        let params = [
            "email": email,
            "user_name": name,
            "gender": gender,
            "phone_number": phone
        ]
        HTTPClient.sharedInstance.PATCH(query, params: params) { (result, error) -> Void in
            if let error = error {
                completionHandler(result: nil, error: error)
            }
            else {
                self.loggedUser = Mapper<User>().map(result["user"])
                
                // Post notification to notify that user logged in
                NSNotificationCenter.defaultCenter().postNotificationName(FoodTrackerUpdateUserNotification, object: self, userInfo: nil)
                
                completionHandler(result: result, error: nil)
                print(result)
            }
        }
        
    }
    
    // Change user password
    func changeUserPasswordWithOldPassword(oldPwd: String, pwdNew: String, pwdConfirmation: String, completionHandler: CompleteHandler) {
        let query = "users/\(loggedUser.userId)/change_password"
        let params = [
            "old_password": oldPwd,
            "new_password": pwdNew,
            "password_confirmation": pwdConfirmation,
        ]
        
        HTTPClient.sharedInstance.POST(query, params: params) { (result, error) -> Void in
            if let error = error {
                completionHandler(result: nil, error: error)
            }
            else {
                completionHandler(result: result, error: nil)
                print(result)
            }
        }
    }
    
    // Upload user avatar
    func uploadUserAvatar(avatar: UIImage, completionHandler: CompleteHandler) {
        let query = "users/\(loggedUser.userId)/upload"
        
        HTTPClient.sharedInstance.uploadImage(query, image: avatar, key: "avatar") { (result, error) -> Void in
            if let error = error {
                completionHandler(result: nil, error: error)
            }
            else {
                let user = result.valueForKeyPath("user.user")
                self.loggedUser = Mapper<User>().map(user)
                
                // Post notification to notify that user logged in
                NSNotificationCenter.defaultCenter().postNotificationName(FoodTrackerUpdateUserNotification, object: self, userInfo: nil)
                
                completionHandler(result: result, error: nil)
                print("User: \(result)")
            }
        }
    }
    
    // Logout user
    func logoutUserWithCompletionHandler(completionHandler: CompleteHandler) {
        HTTPClient.sharedInstance.DELETE("users/sign_out", params: nil) { (result, error) -> Void in
            if let error = error {
                completionHandler(result: nil, error: error)
            }
            else {
                DataManager.sharedInstance.logout()
                completionHandler(result: result, error: nil)
                print(result)
            }
        }
    }
    
    // Get user info
    func getUserInfoById(userId: Int, token: String, completionHandler: CompleteHandler) {
        let query = "users/user_id=\(userId)"
        
        HTTPClient.sharedInstance.GET(query, params: nil) { (result, error) -> Void in
            if let error = error {
                completionHandler(result: nil, error: error)
            }
            else {
                
                self.loggedUser = Mapper<User>().map(result["user"])
                
                // Post notification to notify that user logged in
                NSNotificationCenter.defaultCenter().postNotificationName(FoodTrackerUpdateUserNotification, object: self, userInfo: nil)
                
                completionHandler(result: result, error: nil)
                print(result)
            }
        }
    }
    
    // Favorite a trucker
    func favoriteTrucker(truckerId: Int, completionHandler: CompleteHandler) {
        let params = ["truck_id": truckerId]
        HTTPClient.sharedInstance.POST("socials/like", params: params) { (result, error) -> Void in
            if let error = error {
                
                // Handle data in list of open truckers
                for trucker in self.openTruckers {
                    if let trucker = trucker as? Trucker {
                        if trucker.truckerId == truckerId {
                            trucker.liked = true
                            break;
                        }
                    }
                }
                
                // Handle data in list of close truckers
                for trucker in self.closeTruckers {
                    if let trucker = trucker as? Trucker {
                        if trucker.truckerId == truckerId {
                            trucker.liked = true
                            break;
                        }
                    }
                }
                
                // Handle data in list of favorited truckers
                for trucker in self.favoritedTruckers {
                    if let trucker = trucker as? Trucker {
                        if trucker.truckerId == truckerId {
                            trucker.liked = true
                            break;
                        }
                    }
                }
                
                let userInfo: [String: AnyObject] = ["truckerId": truckerId, "liked": true]
                NSNotificationCenter.defaultCenter().postNotificationName(FoodTrackerUpdateTruckerNotification,
                    object: self, userInfo: userInfo)
                
                completionHandler(result: nil, error: error)
            }
            else {
                completionHandler(result: result, error: nil)
            }
        }
    }
    
    // Remove favorite a trucker
    func removeFavoriteTrucker(truckerId: Int, completionHandler: CompleteHandler) {
        let params = ["truck_id": truckerId]
        HTTPClient.sharedInstance.POST("socials/unlike", params: params) { (result, error) -> Void in
            if let error = error {
                
                // Handle data in list of open truckers
                for trucker in self.openTruckers {
                    if let trucker = trucker as? Trucker {
                        if trucker.truckerId == truckerId {
                            trucker.liked = false
                            break;
                        }
                    }
                }
                
                // Handle data in list of close truckers
                for trucker in self.closeTruckers {
                    if let trucker = trucker as? Trucker {
                        if trucker.truckerId == truckerId {
                            trucker.liked = false
                            break;
                        }
                    }
                }
                
                // Handle data in list of favorited truckers
                for index in 0..<self.favoritedTruckers.count {
                    let trucker = self.favoritedTruckers[index] as! Trucker
                    if trucker.truckerId == truckerId {
                        self.favoritedTruckers.removeObjectAtIndex(index)
                        break;
                    }
                }
                
                let userInfo: [String: AnyObject] = ["truckerId": truckerId, "liked": false]
                NSNotificationCenter.defaultCenter().postNotificationName(FoodTrackerUpdateTruckerNotification,
                    object: self, userInfo: userInfo)
                completionHandler(result: nil, error: error)
            }
            else {
                completionHandler(result: result, error: nil)
            }
        }
    }
    
    // Rate and comment a trucker
    func reviewTrucker(truckerId: Int, rate: Int, reviewContent: String, completionHandler: CompleteHandler) {
        let params = [
            "truck_id": String(truckerId),
            "rate": String(rate),
            "comment": reviewContent
        ]
        
        HTTPClient.sharedInstance.POST("socials/comment", params: params) { (result, error) -> Void in
            if let error = error {
                completionHandler(result: nil, error: error)
            }
            else {
                completionHandler(result: result, error: nil)
                print(result)
            }
        }
    }
    
    // Get country list
    func getCountries(completionHandler: CompleteHandler) {
        HTTPClient.sharedInstance.GET("searchs/countries_index", params: nil) { (result, error) -> Void in
            if let error = error {
                completionHandler(result: nil, error: error)
            }
            else {
                self.countries = Mapper<Country>().mapArray(result["objects"])!
                completionHandler(result: result, error: nil)
                print(result)
                if self.countries.count > 0 {
                    let country = self.countries[0]
                    self.getCitiesByCountryId(country.countryId,
                        completionHandler: { (result, error) -> Void in
                        // Do nothing
                    })
                }
            }
        }
    }
    
    // Get city list by a country
    func getCitiesByCountryId(countryId: Int, completionHandler: CompleteHandler) {
        let query = "searchs/cities_index?country_id=\(countryId)"
        HTTPClient.sharedInstance.GET(query, params: nil) { (result, error) -> Void in
            if let error = error {
                completionHandler(result: nil, error: error)
            }
            else {
                let cities = Mapper<City>().mapArray(result["objects"])!
                for (index, country) in self.countries.enumerate() {
                    if country.countryId == countryId {
                        country.cities = cities
                        self.countries[index] = country
                        break
                    }
                }
                completionHandler(result: cities, error: nil)
                print(result)
            }
        }
    }
    
    // Get food type list
    func getFoodTypeList(completionHandler: CompleteHandler) {
        HTTPClient.sharedInstance.GET("trucks/food_type_list", params: nil) { (result, error) -> Void in
            if let error = error {
                completionHandler(result: nil, error: error)
            }
            else {
                self.foodTypeList = Mapper<FoodType>().mapArray(result["objects"])!
                completionHandler(result: result, error: nil)
                print(result)
            }
        }

    }
    
    // Get local cities by country
    func getLocalCitiesByCountry(countryId: Int) -> [City] {
        var cities = [City]()
        for country in self.countries {
            if country.countryId == countryId {
                cities = country.cities
                break
            }
        }
        
        return cities
    }
    
    // Get local truck category
    func getLocalCategories() {
        
        let categoriesDict = NSArray.readArraybyFileJSON("truck_categories")!
        self.categories = Mapper<Category>().mapArray(categoriesDict)!
        print(categories)
    }
    
    // Get name of category based on category type
    func truckTypeNameFromType(truckType: String) -> String {
        for (_, category) in self.categories.enumerate() {
            if truckType == category.categoryType {
                return category.categoryName
            }
        }
        
        return ""
    }
    
    // Log out user
    func logout() {
        isAuthorized = false
        truckers.removeAll()
        openTruckers.removeAllObjects()
        closeTruckers.removeAllObjects()
        favoritedTruckers.removeAllObjects()
        countries.removeAll()
        loggedUser.logout()
        
        // Remove user
        let userDefault = NSUserDefaults.standardUserDefaults()
        userDefault.removeObjectForKey(FoodTrackerUserIdKey)
        userDefault.removeObjectForKey(FoodTrackerUserTokenKey)
    }
}
