//
//  FoodType.swift
//  FoodTracker
//
//  Created by An Phan on 12/11/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class FoodType: Mappable {
    var foodTypeId: Int = 0
    var foodTypeName = ""
    var foodTypeDesc = ""
    
    required init() {
        
    }
    
    required init?(_ map: Map) {
    }
    
    func mapping(map: Map) {
        foodTypeId <- map["id"]
        foodTypeName <- map["name"]
        foodTypeDesc <- map["description"]
    }
}
