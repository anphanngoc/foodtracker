//
//  GMSMapView.swift
//  FoodTracker
//
//  Created by An Phan on 11/26/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation

extension GMSMapView {
    func configureMarkersForTrucker(trucker: Trucker) {
        let truckerCoordinate = CLLocationCoordinate2D(latitude: Double(trucker.address.latitude)!,
            longitude: Double(trucker.address.longitude)!)
        let originMarker = GMSMarker(position: truckerCoordinate)
        originMarker.map = self
        originMarker.title = trucker.truckername
        originMarker.snippet = trucker.address.street
        originMarker.userData = trucker
        var imageName = trucker.truckType
        if imageName == "cafe" {
            imageName = "popup_cafe"
        }
        else if imageName == "restaurant" {
            imageName = "popup_restaurant"
        }
        originMarker.icon = UIImage(named: imageName)
    }
}
