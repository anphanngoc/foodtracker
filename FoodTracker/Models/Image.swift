//
//  Image.swift
//  FoodTracker
//
//  Created by An Phan on 11/21/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import ObjectMapper

class Image: Mappable {
    var original = ""
    var large = ""
    var medium = ""
    var small = ""
    
    required init() {
        
    }
    
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        original    <- map["url"]
        large       <- map["large.url"]
        medium      <- map["medium.url"]
        small       <- map["small.url"]
    }
}
