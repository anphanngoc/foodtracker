//
//  Item.swift
//  FoodTracker
//
//  Created by An Phan on 11/21/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import ObjectMapper

class Item: Mappable {
    var itemId = 0
    var itemName = ""
    var itemDesc = ""
    var itemPrice = ""
    var itemImage: Image?
    var currency = ""
    
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        itemId         <- map["id"]
        itemName       <- map["name"]
        itemDesc       <- map["description"]
        itemPrice      <- map["price"]
        itemImage      <- map["photo.photo"]
        currency       <- map["currency"]
    }
}
