//
//  Trucker.swift
//  FoodTracker
//
//  Created by An Phan on 11/21/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import ObjectMapper

class Trucker: Mappable {
    var truckerId = 0
    var truckername = ""
    var truckerDecs = ""
    var phone: String = ""
    var cellPhone: String?
    var website: String = ""
    var email: String = ""
    var status: String?
    var startTime: String?
    var endTime: String?
    var averageRate = 0.0
    var liked = false
    var what_new = ""
    var truckType: String = ""
    var foodType = FoodType()
    var address = Address()
    var items = [Item]()
    var truckerImages = [TruckerImage]()
    var comments = [Comment]()
    
    required init() {
        
    }
    
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        truckerId   <- map["id"]
        truckername <- map["truck_name"]
        truckerDecs <- map["description"]
        phone       <- map["phone"]
        cellPhone   <- map["cell_phone"]
        website     <- map["website"]
        email       <- map["truck_email"]
        status      <- map["truck_status"]
        startTime   <- map["str_start_time"]
        endTime     <- map["str_end_time"]
        averageRate <- map["average_rate"]
        liked       <- map["is_liked"]
        what_new    <- map["what_new"]
        truckType   <- map["truck_type"]
        foodType    <- map["food_type_obj"]
        address     <- map["address"]
        items       <- map["sub_menus"]
        truckerImages <- map["photos"]
        comments    <- map["comments"]
    }
}
