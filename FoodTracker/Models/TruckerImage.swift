//
//  TruckerImage.swift
//  FoodTracker
//
//  Created by An Phan on 11/21/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import ObjectMapper

class TruckerImage: Mappable {
    var imageId: Int?
    var position: Int?
    var image: Image?
    
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        imageId     <- map["id"]
        position    <- map["position"]
        image       <- map["picture.picture"]
    }
}
