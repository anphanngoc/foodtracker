//
//  Controller.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/10/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation
import ObjectMapper

class User: Mappable {
    var userId = 0
    var userName = ""
    var email = ""
    var phone = ""
    var gender = ""
    var token = ""
    var avatar = Image()

    required init() {
        
    }
    
    required init?(_ map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        userId         <- map["id"]
        userName       <- map["user_name"]
        email          <- map["email"]
        phone          <- map["phone_number"]
        gender         <- map["gender"]
        token          <- map["token"]
        avatar         <- map["avatar.avatar"]
    }
    
    func logout() {
        userId = 0
        userName = ""
        email = ""
        phone = ""
        gender = ""
        token = ""
        avatar = Image()
    }
}