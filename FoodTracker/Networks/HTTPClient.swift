//
//  Controller.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/10/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

typealias ResponseCompletion = (result: AnyObject!, error: NSError!) -> Void

class HTTPClient {
    // Singleton
    static let sharedInstance = HTTPClient()
    
    // Network manager
    var manager = Alamofire.Manager.sharedInstance
    
    required init() {
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        manager = Alamofire.Manager(configuration: configuration)
    }
    
    func setHeaderToken(token: String){
        var defaultHeaders = self.manager.session.configuration.HTTPAdditionalHeaders ?? [:]
        defaultHeaders["token"] = token
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders = defaultHeaders
        self.manager = Alamofire.Manager(configuration: configuration)
    }
    
    // POST request
    func POST(url: String, params: Dictionary<String, AnyObject>?, completeHandler:ResponseCompletion) {
            let strQuery = "\(BASE_ENDPOINT)\(url)"
            self.manager.request(.POST, strQuery, parameters:params, encoding:ParameterEncoding.URLEncodedInURL)
                .responseJSON(completionHandler: {(response: Response<AnyObject, NSError>) -> Void in
                let result =  response.result
                if let error = result.error {
                    completeHandler(result: nil, error: error)
                } else {
                    if let value: NSDictionary! = result.value as! NSDictionary{
                        if let error = value.arrayForKey("error") {
                            let error = Utilities.getErrorwithDescription(error.firstObject as! String)
                            completeHandler(result: nil, error: error)
                        } else if value.stringForKey("error") != "" {
                            let error = Utilities.getErrorwithDescription(value.stringForKey("error"))
                            completeHandler(result: nil, error:error)
                        } else if value.stringForKey("message") != "" {
                            let error = Utilities.getErrorwithDescription(value.stringForKey("message"))
                            completeHandler(result: nil, error:error)
                        } else {
                            completeHandler(result:value, error: nil)
                            let user = Mapper<User>().map(value)
                            print(user?.userName)
                        }
                        
                    } else {
                        let error = Utilities.getErrorwithDescription("Data not found")
                        completeHandler(result: nil,error: error)
                    }
                }
            })
    }
    
    // GET request
    func GET(url: String, params: Dictionary<String, AnyObject>?, completeHandler:ResponseCompletion) {
        let strQuery = "\(BASE_ENDPOINT)\(url)"
        self.manager.request(.GET, strQuery, parameters:params, encoding:ParameterEncoding.URLEncodedInURL)
            .responseJSON(completionHandler: {(response: Response<AnyObject, NSError>) -> Void in
                let result =  response.result
                if let error = result.error {
                    completeHandler(result: nil, error: error)
                } else {
                    if let value: NSDictionary! = result.value as! NSDictionary{
                        if let error = value.arrayForKey("error") {
                            let error = Utilities.getErrorwithDescription(error.firstObject as! String)
                            completeHandler(result: nil, error: error)
                        } else if value.stringForKey("error") != "" {
                            let error = Utilities.getErrorwithDescription(value.stringForKey("error"))
                            completeHandler(result: nil, error:error)
                        } else if value.stringForKey("message") != "" {
                            let error = Utilities.getErrorwithDescription(value.stringForKey("message"))
                            completeHandler(result: nil, error:error)
                        } else {
                            completeHandler(result:value, error: nil)
                        }
                        
                    } else {
                        let error = Utilities.getErrorwithDescription("Data not found")
                        completeHandler(result: nil, error: error)
                    }
                }
            })
    }
    
    // PATCH request
    func PATCH(url: String, params: Dictionary<String, AnyObject>?, completeHandler:ResponseCompletion) {
        let strQuery = "\(BASE_ENDPOINT)\(url)"
        self.manager.request(.PATCH, strQuery, parameters:params, encoding:ParameterEncoding.URLEncodedInURL)
            .responseJSON(completionHandler: {(response: Response<AnyObject, NSError>) -> Void in
                let result =  response.result
                if let error = result.error {
                    completeHandler(result: nil, error: error)
                } else {
                    if let value: NSDictionary! = result.value as! NSDictionary{
                        if let error = value.arrayForKey("error") {
                            let error = Utilities.getErrorwithDescription(error.firstObject as! String)
                            completeHandler(result: nil, error: error)
                        } else if value.stringForKey("error") != "" {
                            let error = Utilities.getErrorwithDescription(value.stringForKey("error"))
                            completeHandler(result: nil, error:error)
                        } else if value.stringForKey("message") != "" {
                            let error = Utilities.getErrorwithDescription(value.stringForKey("message"))
                            completeHandler(result: nil, error:error)
                        } else {
                            completeHandler(result:value, error: nil)
                        }
                        
                    } else {
                        let error = Utilities.getErrorwithDescription("Data not found")
                        completeHandler(result: nil, error: error)
                    }
                }
            })
    }
    
    // DELELTE request
    func DELETE(url: String, params: Dictionary<String, AnyObject>?, completeHandler:ResponseCompletion) {
        let strQuery = "\(BASE_ENDPOINT)\(url)"
        self.manager.request(.DELETE, strQuery, parameters:params, encoding:ParameterEncoding.URLEncodedInURL)
            .responseJSON(completionHandler: {(response: Response<AnyObject, NSError>) -> Void in
                let result =  response.result
                if let error = result.error {
                    completeHandler(result: nil, error: error)
                } else {
                    if result.value is NSNull {
                        completeHandler(result: "Logout successfully!", error: nil)
                        return;
                    }
                    
                    if let value: NSDictionary! = result.value as! NSDictionary {
                        if let error = value.arrayForKey("error") {
                            let error = Utilities.getErrorwithDescription(error.firstObject as! String)
                            completeHandler(result: nil, error: error)
                        } else if value.stringForKey("error") != "" {
                            let error = Utilities.getErrorwithDescription(value.stringForKey("error"))
                            completeHandler(result: nil, error:error)
                        } else if value.stringForKey("message") != "" {
                            let error = Utilities.getErrorwithDescription(value.stringForKey("message"))
                            completeHandler(result: nil, error:error)
                        } else {
                            completeHandler(result:value, error: nil)
                        }
                        
                    } else {
                        let error = Utilities.getErrorwithDescription("Data not found")
                        completeHandler(result: nil, error: error)
                    }
                }
            })
    }
    
    // Upload
    func uploadImage(url: String, image: UIImage, key: String, completionHandler: ResponseCompletion) {
        let data = image.convertImagetoJPEGData()
        let strQuery = "\(BASE_ENDPOINT)\(url)"
        let fileName = NSString(format: "%@.jpg", NSDate(timeIntervalSinceNow: 0))
        
        self.manager.upload(.POST, strQuery, multipartFormData:{ multipartFormData in
            multipartFormData.appendBodyPart(data:data, name:key, fileName:fileName as String, mimeType:"image/jpeg")
            }, encodingMemoryThreshold:Manager.MultipartFormDataEncodingMemoryThreshold, encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { response in
                        if response.result.error == nil {
                            completionHandler(result: response.result.value, error: nil)
                        } else {
                            completionHandler(result:nil, error: response.result.error)
                        }
                    }
                case .Failure(let encodingError):
                    print(encodingError)
                }
            }
        )
    }
    
    // Download
    func downloadImagefromURL(url:String, completionHandler: ResponseCompletion) {
        self.manager.request(.GET,url, parameters:nil)
            .response { request, response, data, error in
                if error == nil {
                    let image = UIImage(data:data!)
                    completionHandler(result: image, error: nil)
                } else {
                    completionHandler(result: nil, error: error)
                }
        }
    }
}