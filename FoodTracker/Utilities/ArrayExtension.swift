//
//  ArrayExtension.swift
//  FoodTracker
//
//  Created by An Phan on 11/29/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation

extension NSArray {
    class func readArraybyFileJSON(name: String) -> NSArray? {
        if let path = NSBundle.mainBundle().pathForResource(name, ofType: "json") {
            var data: NSData? = nil
            do {
                data = try NSData(contentsOfFile: path, options: NSDataReadingOptions())
            } catch { return nil }
            if let dataDict = data {
                var array: NSArray?
                do {
                    array = try NSJSONSerialization.JSONObjectWithData(dataDict, options: NSJSONReadingOptions()) as? NSArray
                } catch {
                    return nil
                }
                return array
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
}