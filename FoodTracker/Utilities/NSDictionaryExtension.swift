//
//  NSDictionaryExtension.swift
//  FoodTrackerOwner
//
//  Created by Chi Phuong on 11/2/15.
//  Copyright © 2015 Chi Phuong. All rights reserved.
//

import Foundation

extension NSDictionary {
    
    func trimString(string:String!) -> String {
        if string != nil {
            let str = string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            return str
        } else {
            return ""
        }
    }
    
    func arrayForKey(key:String) -> NSArray! {
        if let object = objectForKey(key) {
            if object is NSArray {
                return object as! NSArray
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    func dictionaryForKey(key:String) -> NSDictionary! {
        if let object = objectForKey(key) {
            if object is NSDictionary {
                return object as! NSDictionary
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    func stringForKey(key:String) -> String {
        if objectForKey(key) is NSNull || objectForKey(key) == nil {
            return ""
        } else {
            return objectForKey(key) as! String
        }
        
    }
    
    
    func intForKey(key:String) -> Int {
        if objectForKey(key) is NSNull || objectForKey(key) == nil {
            return 0
        } else {
            return objectForKey(key) as! Int
        }
    }
    func boolForKey(key:String) -> Bool {
        if objectForKey(key) is NSNull || objectForKey(key) == nil{
            return false
        } else {
            return objectForKey(key) as! Bool
        }
     }
    
    func floatForKey(key:String) -> Float {
        if objectForKey(key) is NSNull || objectForKey(key) == nil {
            return 0
        } else {
            return objectForKey(key) as! Float
        }
    }
    
    func doubleForKey(key:String) -> Double {
        if objectForKey(key) is NSNull || objectForKey(key) == nil {
            return 0
        } else {
            return objectForKey(key) as! Double
        }
    }
    


    
    
}