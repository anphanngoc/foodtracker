//
//  UIButtonExtension.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/15/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation
import UIKit

import UIKit

extension UIButton {
    
    func disable() {
        self.enabled = false
        self.alpha = 0.5
    }
    
    func enable() {
        self.enabled = true
        self.alpha = 1
    }
}