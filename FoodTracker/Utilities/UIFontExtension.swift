//
//  UIFontExtension.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/12/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

/**
* Custom category of UIFont to load the custom fonts according to texts.
*/

extension UIFont {
    
    /// useful macros
    
    /**
    * Open Sans font type
    * @params: s size of font
    * @return UIFont font to use
    */
    class func openSansRegular(size s: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans", size: s)!
    }
    
    class func openSansSemibold(size s: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Semibold", size: s)!
    }
}
