//
//  UIImageExtension.swift
//  FoodTracker
//
//  Created by An Phan on 11/22/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation

extension UIImage {
    func convertImagetoJPEGData() -> NSData {
        return UIImageJPEGRepresentation(self, 0.5)!
    }
    
    func convertImagetoPNGData() -> NSData {
        return UIImagePNGRepresentation(self)!
    }
}
