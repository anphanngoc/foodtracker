//
//  UIViewControllerExtension.swift
//  FoodTracker
//
//  Created by An Phan on 11/12/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import Foundation
import UIKit
import RNActivityView

extension UIViewController {
    
    func showOkeyMessage(title: String?, message: String?) {
        
        let alertViewController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertViewController.addAction(UIAlertAction(title: "OK", style: .Cancel) { (alertAction) -> Void in })
        
        presentViewController(alertViewController, animated: true, completion: nil)
    }
    
    func showAlertWithActions(title: String?, message: String?, actions: [UIAlertAction]?) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        if let uActios = actions {
            for action in uActios {
                alert.addAction(action)
            }
        }
        else {
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .Default, handler: { (alertAction) -> Void in }))
        }
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    // MARK: - Progress
    func showLoading() {
        self.view.showActivityView()
    }
    
    func hideLoading() {
        dispatch_async(dispatch_get_main_queue()){
            self.view.hideActivityView()
        }
    }
}