//
//  Utilities.swift
//  FoodTracker
//
//  Created by An Phan on 11/19/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import CoreTelephony

class Utilities: NSObject {
    class func getErrorwithDescription(description: String) -> NSError {
        return NSError(domain:FOOD_TRUCKER, code:100, userInfo:[NSLocalizedDescriptionKey: description])
    }
    
    class func getErrorMessage(error: NSError) -> String {
        let errorString = error.localizedDescription
        return errorString
    }
}

func getIsoCountryCode() -> String! {
    let networkInfo = CTTelephonyNetworkInfo()
    let carrier = networkInfo.subscriberCellularProvider
    if let countryCode = carrier?.isoCountryCode {
        return countryCode
    }
    
    return ""
}

func splitCountryCodeinPhoneNumber(phoneNumber: String) -> (countrycode: String, phone: String) {
    if phoneNumber != "" {
        let array = phoneNumber.componentsSeparatedByString(")")
        if array.count >= 2 {
            let string = array[0]
            let countryCode = string.substringFromIndex(string.startIndex.successor())
            let phone = array[1]
            
            return (countryCode, phone)
        }
        else {
            return ("", phoneNumber)
        }
        
    } else {
        return("","")
    }
}
