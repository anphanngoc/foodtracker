//
//  FoodTruckerTableViewCell.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/15/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

let foodTruckCellID = "FoodTruckCellID"

class FoodTruckerTableViewCell: UITableViewCell {

    @IBOutlet weak var truckImageView: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rateButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        rateButton.layer.cornerRadius = 5.0
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        if selected {
            rateButton.backgroundColor = UIColor.rgbColor(red: 255, green: 168, blue: 0)
            truckImageView.backgroundColor = UIColor.mainColor()
        }
    }
    
    override func setHighlighted(highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        if highlighted {
            rateButton.backgroundColor = UIColor.rgbColor(red: 255, green: 168, blue: 0)
            truckImageView.backgroundColor = UIColor.mainColor()
        }
    }
}
