//
//  InputTableViewCell.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/12/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

protocol InputTableViewCellDelegate {
    func inputCellGenderSwitchDidChangedValue(sender: AnyObject)
}

class InputTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var textField: UITextField?
    @IBOutlet weak var termOfUseLabel: UILabel?
    @IBOutlet weak var genderSwitch: SevenSwitch!
    @IBOutlet weak var dialCodeTextField: UITextField!
    var delegate: InputTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel?.font = UIFont.openSansRegular(size: 14);
        self.titleLabel?.textColor = UIColor.rgbColor(red: 85, green: 106, blue: 120)
        
        self.textField?.font = UIFont.openSansRegular(size: 14);
        
        let name = NSMutableAttributedString(string: "I agree to Term of use")
        let nameRange: NSRange = "I agree to Term of use".rangeOfString("Term of use")
        name.addAttribute(NSForegroundColorAttributeName, value: UIColor.rgbColor(red: 218.0, green: 80.0, blue: 14.0), range: nameRange)
        termOfUseLabel?.attributedText = name
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - IBActions
    
    @IBAction func genderSwitchDidChangedValue(sender: AnyObject) {
        print("on \(genderSwitch.on)")
        delegate?.inputCellGenderSwitchDidChangedValue(sender)
    }
}
