//
//  MenuTableViewCell.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/28/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    @IBOutlet weak var foodImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var foodNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        foodImageView.layer.cornerRadius = CGRectGetWidth(foodImageView.frame) / 2
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
