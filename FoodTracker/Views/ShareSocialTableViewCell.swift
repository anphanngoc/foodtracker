//
//  ShareSocialTableViewCell.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/28/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

protocol ShareSocialTableViewCellDelegate {
    func shareSocialDidTouchedOnFBButton(button: AnyObject)
    func shareSocialDidTouchedOnTwitterButton(button: AnyObject)
    func shareSocialDidTouchedOnEmailButton(button: AnyObject)
    func shareSocialDidTouchedOnWhatsAppButton(button: AnyObject)
}


class ShareSocialTableViewCell: UITableViewCell {
    
    var delegate: ShareSocialTableViewCellDelegate?

    @IBAction func facebookButtonDidTouched(sender: AnyObject) {
        delegate?.shareSocialDidTouchedOnFBButton(sender)
    }
    
    @IBAction func twitterButtonDidTouched(sender: AnyObject) {
        delegate?.shareSocialDidTouchedOnTwitterButton(sender)
    }
    
    @IBAction func emailButtonDidTouched(sender: AnyObject) {
        delegate?.shareSocialDidTouchedOnEmailButton(sender)
    }
    @IBAction func whatsappButtonDidTouched(sender: AnyObject) {
        delegate?.shareSocialDidTouchedOnWhatsAppButton(sender)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
