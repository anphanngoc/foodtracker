//
//  ShowMoreTableViewCell.swift
//  FoodTracker
//
//  Created by An Phan on 12/7/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit

let ShowMoreCellID = "ShowMoreCellID"

class ShowMoreTableViewCell: UITableViewCell {
    
    var activityIndicator: UIActivityIndicatorView!

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.clearColor()
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = contentView.center
        contentView.addSubview(activityIndicator)
    }
    
    required init?(coder aDecoder: NSCoder) {
        // fatalError("init(coder:) has not been implemented")
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        activityIndicator.center = contentView.center
    }

}
