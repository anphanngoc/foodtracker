//
//  TruckerInfoTableViewCell.swift
//  FoodTracker
//
//  Created by An N. PHAN on 10/15/15.
//  Copyright © 2015 Agilabs. All rights reserved.
//

import UIKit
import KASlideShow

protocol TruckerInfoTableViewCellDelegate {
    func truckerCellDidTouchedOnContactButton(button: UIButton)
    func truckerCellDidTouchedOnFavoriteButton(button: DOFavoriteButton)
    func truckerCellDidTouchedOnDirectionButton(button: UIButton)
}

class TruckerInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var favoriteImageButton: DOFavoriteButton!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var contactImageButton: UIButton!
    @IBOutlet weak var directionButton: UIButton!
    @IBOutlet weak var captionContainerView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var rateButton: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var carouselView: KASlideShow!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var topBgView: UIView!
    
    var delegate: TruckerInfoTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        captionContainerView.backgroundColor = UIColor.rgbaColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.7)
        rateButton.layer.cornerRadius = 5.0
        rateButton.titleLabel?.font = UIFont.openSansRegular(size: 14)
        contactButton.titleLabel?.font = UIFont.openSansRegular(size: 17)
        favoriteButton.titleLabel?.font = UIFont.openSansRegular(size: 17)
        directionButton.titleLabel?.font = UIFont.openSansRegular(size: 17)
        
        if DeviceType.IS_IPHONE_6 {
            favoriteButton.setTitle("Follow   ", forState: UIControlState.Normal)
            favoriteButton.setTitle("Followed   ", forState: UIControlState.Selected)
        }
        else if DeviceType.IS_IPHONE_6P {
            favoriteButton.setTitle("Follow      ", forState: UIControlState.Normal)
            favoriteButton.setTitle("Followed      ", forState: UIControlState.Selected)
        }
        
        mapView.bringSubviewToFront(captionContainerView)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        let shadowPath = UIBezierPath(rect: topBgView.bounds)
        topBgView.layer.masksToBounds = false
        topBgView.layer.shadowColor = UIColor.blackColor().CGColor
        topBgView.layer.shadowOffset = CGSize(width: 0, height: 3.0)
        topBgView.layer.shadowOpacity = 0.5
        topBgView.layer.shadowPath = shadowPath.CGPath
    }

    func setInfoTrucker(trucker: Trucker) {
        nameLabel.text = trucker.truckername
        if !trucker.foodType.foodTypeName.isEmpty {
            addressLabel.text = DataManager.sharedInstance.truckTypeNameFromType(trucker.truckType) + " - " + trucker.foodType.foodTypeName
        }
        else {
            addressLabel.text = DataManager.sharedInstance.truckTypeNameFromType(trucker.truckType)
        }
        setFavoriteTrucker(trucker.liked)
        rateButton.setTitle("\(trucker.averageRate)", forState: UIControlState.Normal)
        
        if trucker.address.latitude.characters.count > 0
            && trucker.address.longitude.characters.count > 0 {
            let location = CLLocation(latitude: Double(trucker.address.latitude)!, longitude: Double(trucker.address.longitude)!)
            zoomMaptoLocation(location)
            
            mapView.configureMarkersForTrucker(trucker)
        }
        
        // Disable contact button if phone number is empty
        guard trucker.phone.characters.count > 0 else {
            contactButton.alpha = 0.6
            contactImageButton.alpha = 0.6
            contactButton.userInteractionEnabled = false
            contactImageButton.userInteractionEnabled = false
            return
        }
        
        // TODO: Need to enhance later
        carouselView.imagesContentMode = UIViewContentMode.ScaleAspectFill
        carouselView.delay = 5.0
        carouselView.transitionDuration = 0.5
        carouselView.transitionType = KASlideShowTransitionType.Slide
        carouselView.bringSubviewToFront(rightButton)
        carouselView.bringSubviewToFront(leftButton)
        if trucker.truckerImages.count > 0 {
            self.carouselView.images.removeAllObjects()
            for i in 0..<trucker.truckerImages.count {
                let urlImage = trucker.truckerImages[i].image!.original
                HTTPClient.sharedInstance.downloadImagefromURL(urlImage, completionHandler: { (result, error) -> Void in
                    self.carouselView.addImage(result as! UIImage)
                })
            }
        }
        else {
            if carouselView.images.count == 0 {
                carouselView.addImage(UIImage(named: "carouselPlaceholder"))
                leftButton.hidden = true
                rightButton.hidden = true
            }
        }
        
        if trucker.truckerImages.count <= 1 {
            leftButton.hidden = true
            rightButton.hidden = true
        }
        else {
            leftButton.hidden = false
            rightButton.hidden = false
        }
    }
    
    // MARK: - IBActions
    @IBAction func favoriteButtonDidTouched(sender: DOFavoriteButton) {
        if favoriteImageButton.selected {
            favoriteImageButton.deselect()
            favoriteButton.selected = false
        }
        else {
            favoriteImageButton.select()
            favoriteButton.selected = true
        }
        
        delegate?.truckerCellDidTouchedOnFavoriteButton(sender)
    }
    
    @IBAction func directionButtonDidTouched(sender: AnyObject) {
        delegate?.truckerCellDidTouchedOnDirectionButton(sender as! UIButton)
    }
    
    @IBAction func contactButtonDidTouched(sender: AnyObject) {
        delegate?.truckerCellDidTouchedOnContactButton(sender as! UIButton)
    }
    
    func setFavoriteTrucker(favorited: Bool) {
        if favorited {
            favoriteButton.selected = true
            favoriteImageButton.selected = true
        }
        else {
            favoriteButton.selected = false
            favoriteImageButton.selected = false
        }
    }
    
    @IBAction func slideLeftButtonDidTouched(sender: AnyObject) {
        carouselView.previous()
    }
    
    @IBAction func slideRightButtonDidTouched(sender: AnyObject) {
        carouselView.next()
    }
    
    // MARK: - Private methods
    
    private func zoomMaptoLocation(location: CLLocation) {
        let cameraPosition = GMSCameraPosition.cameraWithLatitude(location.coordinate.latitude,
            longitude: location.coordinate.longitude, zoom: 14);
        mapView.camera =  cameraPosition
    }

}
